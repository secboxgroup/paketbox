import paho.mqtt.client as mqtt
import threading
from DATABASE import Database
from WEBSOCKET import Websocket
from PACKAGEBOX import Packagebox



class MqttMainClient:



    def __init__(self, clientname: str, databaseobject: Database, websocketobject: Websocket, packageboxobject: Packagebox, publisher_switch: bool):

        """
            Diese MQTT Klasse fungiert als Event-handler und kümmert sich um die Kommunikation mit dem MQTT-Broker
            Parameter:  clientname: Der Name des Clients, der im Broker angezeigt wird
                        databaseobject: Benötigt ein Datenbankobjekt 
                        websocketobject: Benötigt ein Websocketobjekt
                        packageboxobject: Benötigt ein Packageboxobjekt
                        publisher_switch: Wenn dieser Schalter True ist, wird der Client nur zum 
                        versenden von Nachrichten verwendet (Sehr wichtig, denn sonst wird der Eventmanager von mehreren Objekten
                        ausgeführt)!
        """

        # Erzeuge ein paho-mqtt Objekt mit Clientname, welcher im Broker angezeigt wird.
        self.client = mqtt.Client(client_id = clientname)

        # Wenn dieser Schalter True ist, wird kein Kanal abonniert und
        # die Klasse wird nur zum versenden von Nachrichten verwendet.
        self.publisher_switch = publisher_switch

        # Broker Adresse und Port(Standard MQTT: 1883)
        self._broker_address = "localhost"
        self._port = 1883

        # Übergeben der Callbackfunktionen von Paho.
        self.client.on_message = self.on_message
        self.client.on_connect = self.on_connect

        # Übergabe der zu integrierenden Objekte.
        self.databaseobject = databaseobject
        self.websocketobject = websocketobject
        self.packageboxobject = packageboxobject

        # Platzhalter für Paketnummer
        self.packagenumberused = " "



    # In der Callbackfunktion von Paho soll ein Abonnement zu dem Systemtopic geschehen
    # Dadurch, dass beim verbinden abonniert wird, wird sichergestellt, 
    # dass der Client immer neu abonniert.
    # :param client: Wird automatisch von paho generiert
    # :param userdata: Wird automatisch von paho generiert
    # :param flags: Wird automatisch von paho generiert
    # :param rc: Wird automatisch von paho generiert
    # :return: VOID
    #
    def on_connect(self, client, userdata, flags, rc):

        # Check, ob der Client nur schreibend benutzt wird
        if self.publisher_switch == False:
            self.client.subscribe("SYS/#")
            self.send_message("SYS/PI/TEMPERATURE", "True", 1)



    # on_message reagiert auf einkommende Nachrichten(Paho Bibliothek).
    # :param client: Wird automatisch von paho generiert
    # :param userdata: Wird automatisch von paho generiert
    # :param flags: Wird automatisch von paho generiert
    # :param rc: Wird automatisch von paho generiert
    # :return: VOID
    #
    def on_message(self, client, userdata, msg):
        
        # Wenn geklingelt wird soll in die Datenbank geschrieben werden.
        # Es wird das Licht für 60 Sekunden eingeschaltet.
        # Mit einer Verzögerung von 5 Sekunden soll die Gartentür geöffnet werden.
        if msg.topic == "SYS/ESP/BELL" and msg.payload.decode("utf-8") == "True":
           
            self.websocketobject.send_bell(1)
            self.databaseobject.write_bell_status(1)
            self.activateLight()
            self.timerdooropen = threading.Timer(5, self.open_door)
            self.timerdooropen.start()

        # Reagiert auf die Anfrage der Website(über RESTAPI)
        # und aktiviert das Licht, sowie den Tür timer.
        elif msg.topic == "SYS/API/DOOR" and msg.payload.decode("utf-8") == "True":

            self.activateLight()
            self.timerdooropen = threading.Timer(5, self.open_door)
            self.timerdooropen.start()

        # Auslösen eines Pop-Ups über Websocket, wenn das Licht angeht + DB Eintrag.
        elif msg.topic == "SYS/ESP/LIGHT" and msg.payload.decode("utf-8") == "True":
            print("Light on")
            self.websocketobject.send_light(1)
            self.databaseobject.write_light_status(1)
        
        # Wenn das Licht aus ist, erfolgt ein DB Eintrag.
        elif msg.topic == "SYS/ESP/LIGHT" and msg.payload.decode("utf-8") == "False":
            print("Light off")
            self.databaseobject.write_light_status(0)

        # Sendet Temperatur an die Website + DB Eintrag.
        elif msg.topic == "SYS/ESP/TEMPERATURE":

            self.websocketobject.send_temperature(msg.payload.decode("utf-8"))
            self.databaseobject.write_temperature(float(msg.payload.decode("utf-8")))

            # Überprüft ob die Betriebszeit eingehalten wird und setzt die Status-LED.
            # Falls die Box im Errormodus ist, wird die Zeit nicht geprüft.
            #self.databaseobject.db_lock.acquire()
            #try:
            if  self.databaseobject.get_packagebox_status() != 2:

                if self.databaseobject.check_operating_time() == True:

                    self.send_message("SYS/PI/STATUSLED", "True", 1)
                    self.databaseobject.write_packagebox_status(1)
                    self.websocketobject.send_boxstatus(1)
                    self.send_message("SYS/PI/ERRORLED", "False", 1)

                else:

                    self.send_message("SYS/PI/STATUSLED", "False", 1)
                    self.databaseobject.write_packagebox_status(0)
                    self.websocketobject.send_boxstatus(0)
                    self.send_message("SYS/PI/ERRORLED", "False", 1)
            else: 
                pass
            #finally:
            #    self.databaseobject.db_lock.release()

        # Wenn die Tür offen ist, erfolgt ein DB Eintrag.
        elif msg.topic == "SYS/ESP/DOOR" and msg.payload.decode("utf-8") == "True":

            self.databaseobject.write_door_status(1)

        # Wenn die Tür geschlossen ist, erfolgt ein DB Eintrag.
        elif msg.topic == "SYS/ESP/DOOR" and msg.payload.decode("utf-8") == "False":

            self.databaseobject.write_door_status(0)

        # Wenn der Scanner ein Paket scannt, soll die Paketnummer überprüft werden.
        # Dies soll nur funktioniert werden wenn die Box Aktiv ist.
        elif msg.topic == "SYS/SCANNER":
            self.packagenumberused = msg.payload.decode("utf-8")

            # Versuchen, das Lock zu erwerben, bevor auf die Datenbank zugegriffen wird
            #self.databaseobject.db_lock.acquire()
            #try:
            # Überprüfen, ob die Box aktiv ist
            if self.databaseobject.get_packagebox_status() == 1:

                # Auslösen eines Pop-Ups über Websocket, wenn ein Paket gescannt wurde
                self.websocketobject.send_barcode_scanned(1)

                # Wenn die Paketnummer stimmt, soll die Box geöffnet werden
                if self.databaseobject.check_package_number(self.packagenumberused):
                    self.send_message("SYS/PI/BOXDOORLOCK", "True", 1)
            else:
                pass
            #finally:
                # Stellen Sie sicher, dass das Lock immer freigegeben wird
                #self.databaseobject.db_lock.release()



        # Startet den Timer für das Öffnungslimit der Box, wenn der Riegel geöffnet wurde + DB Eintrag + Website Pop-Up.
        elif msg.topic == "SYS/GPIO/BOXDOORLOCK" and msg.payload.decode("utf-8") == "True":
            
            self.databaseobject.write_packageboxdoor_lock(1)
            self.websocketobject.send_packagebox_opened(1)
            self.databaseobject.write_packageboxdoor_status(1)
            self.packageboxobject.unlockBox()



        # Stoppt den Timer für das Öffnungslimit der Box, wenn der Riegel geschlossen wurde + DB Eintrag.
        elif msg.topic == "SYS/GPIO/BOXDOORLOCK" and msg.payload.decode("utf-8") == "False":
            
            self.databaseobject.write_packageboxdoor_lock(0)
            self.packageboxobject.lockBox()
            print("Paketbox verriegelt")
            print("Diese Paketnummer wird abgefragt in get_delivery_status" + self.packagenumberused)

            # Wenn ein gescanntes Paket existiert, soll der Status  des Pakets aktualisiert werden(geliefert).
            if self.databaseobject.get_delivery_status(self.packagenumberused) == 2:

                self.databaseobject.write_delivery_status(self.packagenumberused, 3)

                # Zurücksetzen des Platzhalters.
                self.packagenumberused = " "
        
    
        
        # Verriegelt die Paketbox wenn die Tür wieder in Position ist + DB Eintrag der Türposition.
        elif msg.topic == "SYS/GPIO/BOXDOORSENSOR" and msg.payload.decode("utf-8") == "True":
            self.databaseobject.write_packageboxdoor_status(0)
            self.send_message("SYS/PI/BOXDOORLOCK", "False", 1)


        # Auslösen eines Pop-Ups über Websocket, wenn der Errorbutton gedrückt wurde.
        # Zudem wird eine Mail versendet(packageboxobject) und ein Datenbankeintrag erstellt.
        elif msg.topic == "SYS/GPIO/ERRORBUTTON" and msg.payload.decode("utf-8") == "True":
            self.websocketobject.send_error_button(1)
            self.websocketobject.send_boxstatus(2)
            self.databaseobject.write_error_button_status(1)
            self.databaseobject.write_packagebox_status(2)
            
            # Setze die Error-LED, setze die Status-LED auf False
            self.send_message("SYS/PI/STATUSLED", "False", 1)
            self.send_message("SYS/PI/ERRORLED", "True", 1)

            # Falls ein Paket gescannt wurde, wird dieses als Fehler markiert.
            if self.databaseobject.get_delivery_status(self.packagenumberused) == 2:
                self.databaseobject.write_delivery_status(self.packagenumberused, 4)



    # Funktion zum senden einer Nachricht über den MQTT Bus.
    # :param topic(str): Der zu schreibende Mqtt Kanal
    # :param message(str): Mqtt Nachricht 
    # :param qos(int):  0 Bei Daten die periodisch geschickt werden
    #                   1 Bei Daten die sicher ankommen sollen(Kann doppelt auftreten)
    #                   2 NUR bei Daten, die ausnahmslos EXAKT 1 mal ankommen sollen 
    # :return: VOID 
    #  
    def send_message(self, topic, message, qos_level: int):
        
        if self.publisher_switch == False:

            # Sende eine Nachricht
            self.client.publish(topic, message, qos = qos_level)

        # Wenn der Client nur zum verschicken von MQTT Nachrichten verwendet wird,
        # soll der Client nur zum Nachrichten versenden eine Verbindung herstellen und diese wieder trennen. 
        else :
            self.connect_to_mqtt_broker()
            self.client.publish(topic, message, qos = qos_level) 
            self.client.disconnect()
        


    # Funktion zum Verbinden mit dem MQTT-Broker.
    # :param: None
    # :return: VOID 
    #  
    def connect_to_mqtt_broker(self):
        self.client.connect(self._broker_address, self._port, 60)



    # Übergabe der Loop-Funktion von Paho.
    # :param: None
    # :return: VOID
    #   
    def loop_forever(self):
        self.client.loop_forever()
    


    # Thread Funktion für das Licht.
    # :param: None
    # :return: VOID   
    #
    def activateLight(self):
        
        self.send_message("SYS/PI/LIGHT","True" , 1)
        # Thread wird erstellt um das Licht an zu lassen (5 Sekunden)
        self.timerlight = threading.Timer(60, self.deactivateLight)

        # Neustart des Timers, falls mehrmals gedrückt wird.
        if self.timerlight.is_alive():                                               
            self.timerlight.cancel() 
        
        # Thread start.
        else:
            self.timerlight.start()                                                     



    # Deaktiviert das Licht.
    # :param: None
    # :return: VOID   
    #
    def deactivateLight(self):
        
        self.send_message("SYS/PI/LIGHT", "False", 1)



    # Thread Funktion für die Tür.
    # :param: None
    # :return: VOID   
    #
    def open_door(self):
        self.send_message("SYS/PI/DOOR","True" , 1)
        # Thread wird erstellt um die Tür offen zulassen (3 Sekunden)
        self.timerdoor = threading.Timer(5, self.close_door)

        # Neustart des Timers, falls mehrmals gedrückt wird.
        if self.timerdoor.is_alive():                                               
            self.timerdoor.cancel() 
        
        # Thread start.
        else:
            self.timerdoor.start()                                                     



    # Schaltet den Türsummer aus.
    # :param: None
    # :return: VOID  
    # 
    def close_door(self):
        
        self.send_message("SYS/PI/DOOR", "False", 1)  