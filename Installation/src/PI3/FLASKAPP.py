from flask import Flask,request
from flask_cors import CORS
from RESTAPI import REST_API



app = Flask(__name__)  # Initialisiert die Flask-Anwendung



class Flaskhandler:



    def __init__(self, rest_api_object: REST_API):
        """
            Diese Klasse ist die Schnittstelle zwischen der RestAPI und der Datenbank.
            Parameter:  rest_api_object: Ein REST_API-Objekt, das die Kommunikation mit der Datenbank ermöglicht.
        """             
        self.rest_api_object = rest_api_object
        CORS(app)
        


        # Hier wird vom Frontend die User daten übergeben welche dann in der Datenbank gespeichert werden
        # :route: /api/userData
        # :method: POST
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        #        
        @app.route('/api/userData', methods=['POST'])
        def userdata():
            Data = request.json
            return self.rest_api_object.get_user_data(Data)



        # das backend gibt die Aktuellen Zeiten zurück an das Frontend
        # :route: /api/showtime
        # :method: GET
        # :return: JSON-Objekt mit der aktuellen Zeit
        #
        @app.route('/api/showtime', methods=['GET'])
        def show_time():
            return self.rest_api_object.show_time()



        # Frontend fragt daten an und bekommt von der Datenbank die ganzen E-Mail Adressen die gespeichert sind 
        # :route: /api/emaillist
        # :method: GET
        # :return: JSON-Objekt mit allen E-Mail-Adressen
        #
        @app.route('/api/emaillist', methods=['GET'])
        def email_list():    
            return self.rest_api_object.email_list()



        # Anfrage vom Frontend das Backend gibt alle Systemlogs zurück 
        # :route: /api/syslog
        # :method: GET
        # :return: JSON-Objekt mit allen Systemlogs
        #
        @app.route('/api/syslog', methods=['GET'])
        def system_log():
            return self.rest_api_object.system_log()



        # Das Frontend hat eine Neues Packet registriert 
        # :route: /api/addpackage
        # :method: POST
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        #
        @app.route('/api/addpackage', methods=['POST'])
        def add_package():
            print("FLASK APP erreicht")
            Data = request.json
            return self.rest_api_object.add_package(Data)
        
        

        # Das Forntend hat ein Packet gelöscht --> weitergeben an die DB das es gelöscht wird
        # :route: /api/deletepackage
        # :method: POST
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        # 
        @app.route('/api/deletepackage', methods=['POST'])
        def delete_package():
            Data = request.json
            return self.rest_api_object.delete_package(Data)

        

        # Nachfrage vom Frontend an die DB. alle Packete müssen übergeben werden
        # :route: /api/allpackages
        # :method: GET
        # :return: JSON-Objekt mit allen Paketen
        #
        @app.route('/api/allpackages', methods=['GET'])
        def all_packages():
            return self.rest_api_object.all_packages()



        # Nachfrage vom Frontend an die DB. alle offene Packete müssen übergeben werden
        # :route: /api/openpackages 
        # :method: GET
        # :return: JSON-Objekt mit allen offenen Paketen
        #
        @app.route('/api/openpackages', methods=['GET'])
        def open_packages():
            return self.rest_api_object.open_packages()



        # Nachfrage von Frontend an die DB--> die Letzten 5 Packete müssen angezeit werden 
        # :route: /api/lastfivepackages
        # :method: GET
        # :return: JSON-Objekt mit den letzten 5 Paketen
        #
        @app.route('/api/lastfivepackages', methods=['GET'])
        def last_five_packages():
            return self.rest_api_object.last_five_packages()



    	# Anweisung vom Frontend das die Gartenpforte nun zu öffnen ist
        # :route: /api/opengardendoor
        # :method: POST
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        #
        @app.route('/api/opengardendoor', methods=['POST'])
        def open_garden_door():
            Data = request.json
            return self.rest_api_object.open_garden_door(Data)



        # Anweisung vom Frontend das die Packetbox nunn zu öffnen ist
        # :route: /api/openpacketbox
        # :method: POST
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        #
        @app.route('/api/openpacketbox', methods=['POST'])
        def open_packet_box():
            Data = request.json
            return self.rest_api_object.open_packet_box(Data)

    

        # Abfrage ob der Benutzer schon in der Datenbank vorhanden ist
        # :route: /api/nachnahme
        # :method: GET
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        #
        @app.route('/api/alreadyRegistered', methods=['GET'])
        def already_registered():
            return self.rest_api_object.already_registered()
    


        # Übergabe von E-Mails die Gelöscht werden sollen an das Backend  
        # :route: /api/deleteemails
        # :method: POST
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        #
        @app.route('/api/deleteemails', methods=['POST'])
        def deleteemails():
            Data = request.json
            print("FLASK APP erreicht")
            return self.rest_api_object.deleteemails(Data)



        # Hier wird vom Frontend die User daten übergeben welche dann in der Datenbank gespeichert werden
        # :route: /api/userDataUpdate
        # :method: POST
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        #
        @app.route('/api/userDataUpdate', methods=['POST'])
        def get_update_user_data():
            Data = request.json
            return self.rest_api_object.get_update_user_data(Data)



        # Umschalten des Uhrzeitenfunktion-Status
        # :route: /api/timesstatus
        # :method: POST
        # :return: JSON-Objekt mit dem Status der Datenbankoperation
        #
        @app.route('/api/timesstatus', methods=['POST'])
        def switchtimes():
            Data = request.json
            return self.rest_api_object.switchtimes(Data)
        


    # Startet die Flask-Anwendung
    # :host: localhost
    # :port: 8000
    #
    def run(self):
        app.run(debug=False, host='localhost', port=8000)