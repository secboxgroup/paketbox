flask             ==  3.0.0
Flask-SocketIO    ==  5.3.6
mysql-connector-python   ==  8.3.0
paho-mqtt         ==  1.6.1
python-dotenv     ==  1.0.0
Flask-Cors        ==  4.0.0
RPi.GPIO          ==  0.7.1    