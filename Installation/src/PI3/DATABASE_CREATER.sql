-- Erstellen der Datenbank, falls sie noch nicht existiert (optional)
CREATE DATABASE IF NOT EXISTS secbox;
USE secbox;

-- Löschen der Tabellen, falls sie existieren (optional)
-- DROP TABLE IF EXISTS Emails;
-- DROP TABLE IF EXISTS PackageData;
-- DROP TABLE IF EXISTS SystemLog;
-- DROP TABLE IF EXISTS Settings;
-- DROP TABLE IF EXISTS Users;

-- Erstellen der Tabelle 'UserData'
CREATE TABLE IF NOT EXISTS UserData (
    UserID INT AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(30),
    lastname VARCHAR(30),
    address VARCHAR(255),
    city VARCHAR(100),
    zip VARCHAR(10)
);

-- Erstellen der Tabelle 'UserEmails'
CREATE TABLE IF NOT EXISTS UserEmails (
    email VARCHAR(100) NOT NULL PRIMARY KEY
);

-- Erstellen der Tabelle 'PackageData'
CREATE TABLE IF NOT EXISTS PackageData (
    packageID INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    packageNumber VARCHAR(25) NOT NULL,
    deliveryStatus VARCHAR(20) NOT NULL,
    deliveryDate VARCHAR(30),
    deliveryTime TIME,
    packageName VARCHAR(25)
);

-- Erstellen der Tabelle 'SystemLog'
CREATE TABLE IF NOT EXISTS SystemLog (
    LogID INTEGER AUTO_INCREMENT PRIMARY KEY,
    Event VARCHAR(200) NOT NULL,
    Timestamp TIMESTAMP NOT NULL
);

-- Erstellen der Tabelle 'Settings'
CREATE TABLE IF NOT EXISTS Settings (
    startTime VARCHAR(5),
    endTime VARCHAR(5),
    temperature FLOAT,
    packageboxStatus INT, 
    packageboxDoorStatus BOOLEAN,
    packageboxDoorLock BOOLEAN,
    doorStatus BOOLEAN,
    light BOOLEAN,
    bell BOOLEAN,
    errorButton BOOLEAN,
    timesStatus BOOLEAN
);

-- Werte initialisieren
INSERT INTO Settings (startTime, endTime, temperature, packageboxStatus, packageboxDoorStatus, packageboxDoorLock, doorStatus, light, bell, errorButton, timesStatus)
VALUES ('00:00', '00:00', 0.0, 0, 0, 0, 0, 0, 0, 0, 0);


-- Benutzer erstellen
CREATE USER 'pi'@'localhost' IDENTIFIED BY 'raspberry';


-- Benutzerrechte vergeben
GRANT SELECT, INSERT, DELETE, UPDATE ON secbox . * TO 'pi'@'localhost';

-- Änderungen übernehmen
FLUSH PRIVILEGES;


