from datetime import datetime, timedelta
from dotenv import load_dotenv
from SENDMAIL import Mail
from WEBSOCKET import Websocket
import os
import mysql.connector
import json
import datetime
#import threading

load_dotenv()


class Database:


    def __init__(self, websocketobject: Websocket, mailobject: Mail):
        """
            Diese Datenbankklasse ist für die Kommunikation mit der Datenbank verantwortlich
            Parameter:  websocketobject: Benötigt ein Websocketobjekt
                        mailobject: Benötigt ein Mailobjekt
        """
        self.conn = mysql.connector.connect(
            host = os.getenv("database_host"),
            user = os.getenv("database_user"),        
            password = os.getenv("database_password"),
            database = os.getenv("database_database")
        )
        #self.db_lock = threading.Lock()


        # Websocketobjekt
        self.websocketobject = websocketobject

        # EMailobjekt
        self.mailobject = mailobject  # Instanz der Mail-Klasse
    


# ==============================================================================================================
# ============== SYSTEMLOG =====================================================================================
# ==============================================================================================================



    # Fügt einen Eintrag in die SystemLog-Tabelle ein.
    # :param event: Beschreibung des Ereignisses, das geloggt werden soll.
    #  
    def log_event(self, event):
        # Aktuelles Datum und Uhrzeit feststellen
        timestamp = datetime.datetime.now()

        # SQL-Befehl vorbereiten
        self.cursor = self.conn.cursor(buffered=True)        
        query = "INSERT INTO SystemLog (Event, Timestamp) VALUES (%s, %s)"

        # Ausführen des SQL-Befehls mit Parametern
        self.cursor.execute(query, (event, timestamp))
        self.conn.commit()
        self.cursor.close()
        
   

    # Löschen der Systemlogs aus der Datenbank (älter als 100 Tage)
    # :return: True, wenn die Logs gelöscht wurden
    #         
    def delete_old_logs(self):
        # Berechnung des Datums vor 100 Tagen
        hundred_days_ago = datetime.datetime.now() - timedelta(days=100)

        # SQL-Befehl vorbereiten
        self.cursor = self.conn.cursor(buffered=True)
        query = "DELETE FROM SystemLog WHERE Timestamp < %s"

        # Ausführen des SQL-Befehls mit Parameter
        self.cursor.execute(query, (hundred_days_ago,))
        self.conn.commit()
        self.cursor.close()
        
        return True



    # Holt alle Systemlogs und gibt sie im JSON-Format zurück.
    # :return: JSON-String mit allen Systemlogs.
    #
    def get_system_logs(self):
        # SQL-Abfrage, um alle Systemlogs zu erhalten
        self.cursor = self.conn.cursor(buffered=True)
        query = "SELECT LogID, Timestamp, Event FROM SystemLog ORDER BY LogID DESC"
        self.cursor.execute(query)
        logs = self.cursor.fetchall()
        self.cursor.close

        # Umwandlung der Log-Daten in das gewünschte JSON-Format
        log_list = []
        for log in logs:
            # Überprüfen, ob Timestamp ein datetime-Objekt ist und entsprechend formatieren
            if isinstance(log[1], datetime.datetime):
                timestamp = log[1].strftime('%b %d %Y %I:%M:%S %p')
            else:
                timestamp = log[1]  # Verwendung des Timestamps als String, falls es kein datetime-Objekt ist

            log_entry = {
                "logId": log[0],
                "time": timestamp,
                "info": log[2]
            }
            log_list.append(log_entry)

        # Umwandlung der Liste in einen JSON-String
        log_list_json = json.dumps(log_list)

        # Systemlog schreiben
        self.log_event("Die Systemlogs wurden abgefragt")
        
        return log_list_json



# ==============================================================================================================
# ============== USERDATEN ===================================================================================== 
# ==============================================================================================================



    # Überprüft, ob bereits ein Benutzer in der Datenbank existiert.   
    # :return: True, wenn ein Benutzer existiert, sonst False
    #
    def check_user(self):
        # SQL-Abfrage, um die Anzahl der Benutzer in der UserData-Tabelle zu zählen
        self.cursor = self.conn.cursor(buffered=True)
        query = "SELECT COUNT(*) FROM UserData"

        # Ausführen der Abfrage und Speichern des Ergebnisses
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()

        # Systemlog schreiben
        self.log_event("Die Benutzeranzahl wurde abgefragt")
        
        # Überprüfen, ob die Anzahl der Benutzer größer als 0 ist
        return result[0] > 0



    # Fügt einen neuen Benutzer und die zugehörigen Daten in die Datenbank ein.
    # :param user_data_json: JSON-String mit Benutzerdaten.
    # :return: True bei Erfolg, False bei einem Fehler.
    #
    def create_user(self, user_data):
        # Userdaten validieren
        if(self.validate_userdata(user_data) == False):
            return False

        firstname = user_data["firstname"]
        lastname = user_data["lastname"]
        address = user_data["address"]
        city = user_data["city"]
        zip = user_data["zip"]

        # Benutzerdaten in UserData einfügen 
        self.cursor = self.conn.cursor(buffered=True)
        query = "INSERT INTO UserData (firstname, lastname, address, city, zip) VALUES (%s, %s, %s, %s, %s)"
        self.cursor.execute(query, (firstname, lastname, address, city, zip))
        self.conn.commit()
        self.cursor.close()

        email = user_data["email"]
        # Email und Betriebszeiten verarbeiten
        self.add_email(email)
        if(user_data["start"] != "" or user_data["end"]):
            start = user_data["start"]
            end = user_data["end"]

            self.write_operating_time(start, end)

        # Überprüfung ob ein Nutzer hinzugefügt werden konnte
        success_user = self.check_user()
        if not (success_user):
             # Systemlog schreiben
            self.log_event("Anlegen eines neuen Benutzers fehlgeschlagen")

            return False

            # Systemlog schreiben
        self.log_event("Neuer Benutzer angelegt")
        
        return True



    # Aktualisiert die Daten eines bestehenden Benutzers.
    # :param user_data_json: JSON-String mit den zu aktualisierenden Benutzerdaten.
    # :return: True bei Erfolg, False bei einem Fehler.
    #
    def update_user(self, user_data):
        # SQL-Update-Statement vorbereiten
        update_fields = []
        params = []

        # Für jedes Feld überprüfen, ob ein Wert vorhanden ist und es zum Update hinzufügen
        if user_data["firstname"]:
            update_fields.append("firstname = %s")
            params.append(user_data["firstname"])
        if user_data["lastname"]:
            update_fields.append("lastname = %s")
            params.append(user_data["lastname"])
        if user_data["address"]:
            update_fields.append("address = %s")
            params.append(user_data["address"])
        if user_data["city"]:
            update_fields.append("city = %s")
            params.append(user_data["city"])
        if user_data["zip"]:
            update_fields.append("zip = %s")
            params.append(user_data["zip"])

        # SQL-Update ausführen
        if len(params) > 0:
            # SQL-Update-Statement zusammenbauen
            self.cursor = self.conn.cursor(buffered=True)
            query = "UPDATE UserData SET " + ", ".join(update_fields)
            # Direkte Ausführung bei nur einem Parameter
            # Bei einem Tupel mit nur einem Eintrag 
            # wird ein " , " hinzugefügt, 
            # was zu einem Fehler führt
            self.cursor.execute(query, (params[0],))
            self.conn.commit()
            self.cursor.close()

            # Systemlog schreiben
            self.log_event("Die Benutzerinformationen wurden geupdated")

        
        # Email und Betriebszeiten aktualisieren
        if (user_data["email"]):
            email = user_data["email"]
            self.add_email(email)
        if(user_data["start"] or user_data["end"]):
            start = user_data["start"]
            end = user_data["end"]

            self.write_operating_time(start, end)
        
        return True
    

    
# ==============================================================================================================
# ============== EMAIL =========================================================================================
# ==============================================================================================================
    


    # Fügt eine neue E-Mail-Adresse in die UserEmails-Tabelle ein, wenn sie nicht bereits existiert.
    # :param email: Die hinzuzufügende E-Mail-Adresse.
    # :return: True, wenn die E-Mail-Adresse hinzugefügt wurde, sonst False.
    #
    def add_email(self, email):
        self.cursor = self.conn.cursor(buffered=True)
        # Überprüfen, ob die E-Mail bereits existiert
        query_this = "SELECT COUNT(*) FROM UserEmails WHERE email = %s"
        self.cursor.execute(query_this, (email,))
        result_this = self.cursor.fetchone()
        self.cursor.close()

        self.cursor = self.conn.cursor(buffered=True)
        query_all = "SELECT COUNT(*) FROM UserEmails"
        self.cursor.execute(query_all)
        result_all = self.cursor.fetchone()
        self.cursor.close()

        # E-Mail nur hinzufügen, wenn sie noch nicht existiert
        if result_this[0] == 0 and result_all[0] == 0:
            self.cursor = self.conn.cursor(buffered=True)
            insert_query = "INSERT INTO UserEmails (email) VALUES (%s)"
            self.cursor.execute(insert_query, (email,))
            self.conn.commit()
            self.cursor.close() 
            self.mailobject.send_Mail(1, str(email))
            return True
        elif result_this[0] == 0 and result_all[0] != 0:
            self.cursor = self.conn.cursor(buffered=True)
            insert_query = "INSERT INTO UserEmails (email) VALUES (%s)"
            self.cursor.execute(insert_query, (email,))
            self.conn.commit()
            self.cursor.close()

            # Systemlog schreiben
            self.log_event(f'Es wurde eine neue E-Mail-Addresse hinzugefügt: {str(email)}')

            # EMail für das anlegen einer neuen Emailadresse versenden
            self.mailobject.send_Mail(2, str(email))
            return True
        


    # Löscht E-Mail-Adressen aus der UserEmails-Tabelle basierend auf einem JSON-Objekt.
    # :param emails_json: JSON-String mit E-Mail-Adressen, die gelöscht werden sollen.
    # :return: True, wenn die E-Mail-Adressen gelöscht wurden, sonst False.
    #
    def delete_emails(self, emails_to_delete):
        self.cursor = self.conn.cursor(buffered=True)
        # Gesamtanzahl der E-Mails in der Tabelle ermitteln
        self.cursor.execute("SELECT COUNT(*) FROM UserEmails")
        total_emails_count = self.cursor.fetchone()[0]
        self.cursor.close()
        # Prüfen, ob es überhaupt EMails zum löschen gibt
        if (int(total_emails_count) != 0):
            # Prüfen, ob die Anzahl der zu löschenden E-Mails noch eine EMail übrig lässt
            # und wenn nein dann stutzen wir das Ding einfach zurecht
            if len(emails_to_delete) >= total_emails_count:
                # Anzahl der zu löschenden E-Mails reduzieren
                emails_to_delete = emails_to_delete[:total_emails_count - 1]

            # E-Mail-Adressen aus der Datenbank löschen, wenn sie vorhanden sind
            for emails in emails_to_delete:
                email = emails
                # Überprüfen, ob die E-Mail-Adresse in der Datenbank existiert
                self.cursor = self.conn.cursor(buffered=True)
                check_query = "SELECT COUNT(*) FROM UserEmails WHERE email = %s"
                self.cursor.execute(check_query, (email,))
                count = self.cursor.fetchone()[0] 
                self.cursor.close()
                if int(count) > 0:
                    # E-Mail-Adresse löschen, wenn sie existiert
                    self.cursor = self.conn.cursor(buffered=True)
                    delete_query = "DELETE FROM UserEmails WHERE email = %s"
                    self.cursor.execute(delete_query, (email,))
                    self.conn.commit()
                    self.cursor.close()

                    # Systemlog schreiben
                    self.log_event(f'Es wurden eine E-Mail-Adresse entfernt: {email}')

            return True
        else:

            return False
            
        
        
    # Abfrage einer Emailliste aus der Datenbank nach abc geordnet
    # :return: JSON-String mit allen E-Mail-Adressen.
    #
    def get_email_list(self):
        # SQL-Abfrage, um alle E-Mail-Adressen zu erhalten
        self.cursor = self.conn.cursor(buffered=True)
        query = "SELECT email FROM UserEmails ORDER BY email ASC"
        self.cursor.execute(query)

        # Ergebnis der Abfrage holen
        emails = self.cursor.fetchall()
        self.cursor.close()

        # Umwandlung in das gewünschte JSON-Format
        email_list = []
        for email in emails:
            email_entry = {"email": email[0]}
            email_list.append(email_entry)

        # Umwandlung der Liste in einen JSON-String
        email_list_json = json.dumps(email_list)

        # Systemlog schreiben
        self.log_event("Die E-Mail-Addressen wurden abgefragt")

        return email_list_json



    # Abfrage einer Emailliste aus der Datenbank nach abc geordnet
    # :return: Array mit allen E-Mail-Adressen.
    #
    def get_email_array(self):
        # SQL-Abfrage vorbereiten
        self.cursor = self.conn.cursor(buffered=True)
        query = "SELECT email FROM UserEmails ORDER BY email ASC"

        # Ausführen der Abfrage
        self.cursor.execute(query)
        results = self.cursor.fetchall()
        self.cursor.close() 

        # Ergebnisse in ein Array von E-Mail-Adressen umwandeln
        email_array = [email[0] for email in results]

        return email_array



# ==============================================================================================================
# ============== PAKETDATEN ====================================================================================
# ==============================================================================================================
    


    # Fügt ein neues Paket in die PackageData-Tabelle ein.
    # :param package_json: JSON-String mit Paketdaten.
    # :return: True, wenn das Paket eingetragen wurde, sonst False.
    #
    def add_package(self, package_data):
        # Validieren
        if not (package_data["name"] or package_data["number"]):
            return False
        
        self.cursor = self.conn.cursor(buffered=True)
        # Prüfen, ob die Paketnummer bereits in der Datenbank existiert
        check_query = "SELECT COUNT(*) FROM PackageData WHERE packageNumber = %s"
        self.cursor.execute(check_query, (package_data["number"],))
        count = self.cursor.fetchone()[0]  # Ergebnis der Abfrage abholen
        self.cursor.close()

        # Aktuelles Datum und Uhrzeit für Lieferdatum und -zeit festlegen
        delivery_date = datetime.date.today().strftime('%d-%m-%Y')
        delivery_time = datetime.datetime.now().strftime('%H:%M')

        if int(count) == 1:
            self.cursor = self.conn.cursor(buffered=True)
            query = "UPDATE PackageData SET packageName = %s WHERE packageNumber = %s"
            self.cursor.execute(query, (package_data["name"], package_data["number"],))
            self.conn.commit()
            self.cursor.close()

            # Systemlog schreiben
            self.log_event(f'Das Paket mit der Packetnummer: {package_data["number"]} wurde geändert')

            return True
        
        elif int(count) == 0 or count == None:
            self.cursor = self.conn.cursor(buffered=True)
            # Paket in die Datenbank einfügen
            insert_query = "INSERT INTO PackageData (packageNumber, deliveryStatus, deliveryDate, deliveryTime, packageName) VALUES (%s, %s, %s, %s, %s)"
            self.cursor.execute(insert_query, (package_data["number"], 1, delivery_date, delivery_time, package_data["name"],))
            self.conn.commit()
            self.cursor.close()

            # Systemlog schreiben
            self.log_event(f'Das Paket mit der Packetnummer: {package_data["number"]} wurde hinzugefügt')

            return True



    # Löscht ein Paket aus der PackageData-Tabelle basierend auf einer Paketnummer.
    # :param package_data: JSON-Objekt mit der Paketnummer.
    # :return: True, wenn das Paket gelöscht wurde, sonst False.
    #
    def delete_package(self, package_data):
        # Validieren
        if not (package_data["number"]):
            return False
        
        package_number = package_data["number"]
    
        self.cursor = self.conn.cursor(buffered=True)
        # Prüfen, ob die Paketnummer in der Datenbank existiert
        check_query = "SELECT COUNT(*) FROM PackageData WHERE packageNumber = %s"
        self.cursor.execute(check_query, (package_number,))
        count = self.cursor.fetchone()[0]
        self.cursor.close()
        if int(count) > 0:
            # Paket löschen, wenn die Paketnummer existiert
            self.cursor = self.conn.cursor(buffered=True)
            delete_query = "DELETE FROM PackageData WHERE packageNumber = %s"
            self.cursor.execute(delete_query, (package_number,))
            self.conn.commit()
            self.cursor.close()

            # Systemlog schreiben
            self.log_event(f'Das Paket mit der Packetnummer: {package_data["number"]} wurde geändert')

        return True

        
        
    # Holt alle offenen Pakete (deliveryStatus = 1) 
    # :return: JSON-String mit allen offenen Paketen.
    #
    def get_open_packages(self):
            # SQL-Abfrage, um alle offenen Pakete zu erhalten
            self.cursor = self.conn.cursor(buffered=True)
            query_packages = "SELECT packageName, packageNumber, deliveryDate, deliveryTime, deliveryStatus FROM PackageData WHERE deliveryStatus = '1'"
            self.cursor.execute(query_packages)
            packages = self.cursor.fetchall()
            self.cursor.close()

            # SQL-Abfrage für allgemeine Benutzer- und Adressinformationen
            self.cursor = self.conn.cursor(buffered=True)
            query_user = "SELECT firstname, lastname, address, zip, city FROM UserData"
            self.cursor.execute(query_user)
            user_data = self.cursor.fetchone()
            self.cursor.close()
            general_user = f"{user_data[0]} {user_data[1]}"
            general_street = f"{user_data[2]}, {user_data[3]} {user_data[4]}"

            # Umwandlung der Paketdaten in das gewünschte JSON-Format
            package_list = []
            for package in packages:
                package_data = {
                    "name": package[0],
                    "number": package[1],
                    "time": package[2],
                    "status": package[4],
                    "street": general_street,
                    "user": general_user
                }
                package_list.append(package_data)

            # Umwandlung der Liste in einen JSON-String
            package_list_json = json.dumps(package_list)

            # Systemlog schreiben
            self.log_event("Die Liste aller offenen Pakete wurde abgefragt.")

            return package_list_json



    # Holt die letzten fünf Pakete 
    # :return: JSON-String mit den letzten fünf Paketen.
    #
    def get_last_five_packages(self):
        # SQL-Abfrage, um die letzten fünf Pakete zu erhalten
        self.cursor = self.conn.cursor(buffered=True)
        query_packages = """
            SELECT packageName, packageNumber, deliveryDate, deliveryTime, deliveryStatus
            FROM PackageData
            ORDER BY deliveryDate DESC, deliveryTime DESC
            LIMIT 5
        """
        self.cursor.execute(query_packages)
        packages = self.cursor.fetchall()
        self.cursor.close()

        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage für allgemeine Benutzer- und Adressinformationen
        query_user = "SELECT firstname, lastname, address, zip, city FROM UserData"
        self.cursor.execute(query_user)
        user_data = self.cursor.fetchone()
        self.cursor.close()
        general_user = f"{user_data[0]} {user_data[1]}"
        general_street = f"{user_data[2]}, {user_data[3]} {user_data[4]}"

        # Umwandlung der Paketdaten in das gewünschte JSON-Format
        package_list = []
        for package in packages:
            package_data = {
                "name": package[0],
                "number": package[1],
                "time": package[2],
                "status": package[4],
                "street": general_street,
                "user": general_user
            }
            package_list.append(package_data)

        # Umwandlung der Liste in einen JSON-String
        package_list_json = json.dumps(package_list)

        # Systemlog schreiben
        self.log_event("Die Liste der letzten 5 Pakete wurde abgefragt.")
        
        return package_list_json
    


    # Holt alle Pakete 
    # :return: JSON-String mit allen offenen Paketen
    #
    def get_all_packages(self):
            # SQL-Abfrage, um alle offenen Pakete zu erhalten
            self.cursor = self.conn.cursor(buffered=True)
            query_packages = "SELECT packageName, packageNumber, deliveryDate, deliveryTime, deliveryStatus FROM PackageData"
            self.cursor.execute(query_packages)
            packages = self.cursor.fetchall()
            self.cursor.close()

            # SQL-Abfrage für allgemeine Benutzer- und Adressinformationen
            self.cursor = self.conn.cursor(buffered=True)
            query_user = "SELECT firstname, lastname, address, zip, city FROM UserData"
            self.cursor.execute(query_user)
            user_data = self.cursor.fetchone()
            self.cursor.close()
            general_user = f"{user_data[0]} {user_data[1]}"
            general_street = f"{user_data[2]}, {user_data[3]} {user_data[4]}"

            # Umwandlung der Paketdaten in das gewünschte JSON-Format
            package_list = []
            for package in packages:
                package_data = {
                    "name": package[0],
                    "number": package[1],
                    "time": package[2],
                    "status": package[4],
                    "street": general_street,
                    "user": general_user
                }
                package_list.append(package_data)

            # Umwandlung der Liste in einen JSON-String
            package_list_json = json.dumps(package_list)

            # Systemlog schreiben
            self.log_event("Die Liste aller Pakete wurde abgefragt.")

            return package_list_json


    
    # Überprüft eine Paketnummer und setzt gegebenenfalls den Status oder sendet E-Mails.
    # :param package_number: Die zu überprüfende Paketnummer.
    # :return: True wenn das Paket eingetragen war, False wenn nicht
    #
    def check_package_number(self, package_number):
        self.cursor = self.conn.cursor(buffered=True)
        # Überprüfen, ob die Paketnummer existiert und der Status 1 ist   
        check_query = "SELECT deliveryStatus FROM PackageData WHERE packageNumber = %s"
        self.cursor.execute(check_query, (package_number,))
        result = self.cursor.fetchone()
        self.cursor.close()

        if result and result[0] == '1':
            # Status von 1 auf 2 setzen
            self.cursor = self.conn.cursor(buffered=True)
            update_query = "UPDATE PackageData SET deliveryStatus = '2' WHERE packageNumber = %s"
            self.cursor.execute(update_query, (package_number,))
            self.conn.commit()
            self.cursor.close()

            # Über Websocket verschicken
            self.websocketobject.send_packagestatus("2", package_number)

            # Systemlog schreiben
            self.log_event(f'Der Status des Pakets mit der Paketnummer: {(package_number,)} wurde geändert')

            return True
        else:
            
            emails = self.get_email_array()

            for mailadresse in emails:
                self.mailobject.send_Mail(3, (mailadresse))
            
            # Systemlog schreiben
            self.log_event(f'Es wurde ein unbekanntes Paket mit der Paketnummer: {(package_number,)} gescannt')

            return False
        


    # Holt den Status und die Paketnummer eines Pakets anhand seiner Paketnummer.
    # :param package_number: Die Paketnummer des abzufragenden Pakets.
    # :return: JSON-String mit Status und Paketnummer des Pakets.
    #
    def get_package_data(self, package_number):
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um den Status und die Paketnummer des Pakets zu erhalten
        query = "SELECT deliveryStatus, packageNumber FROM PackageData WHERE packageNumber = %s"
        self.cursor.execute(query, (package_number,))
        result = self.cursor.fetchone()
        self.cursor.close()

        # Überprüfen, ob ein Ergebnis vorhanden ist
        if result:
            # Ergebnis in das gewünschte JSON-Format umwandeln
            package_data = [{"status": result[0], "number": result[1]}]
            package_data_json = json.dumps(package_data)

            # Systemlog schreiben
            self.log_event(f'Der Status des Pakets mit der Packetnummer: {(package_number,)} wurde abgefragt')
            return package_data_json
        else:
            # Kein Paket mit dieser Nummer gefunden
            return False
        


    # Löscht alle Pakete, die älter als 100 Tage sind.
    #
    def delete_old_packages(self):
        # Datum berechnen, das 100 Tage vor heute liegt
        thirty_days_ago = (datetime.date.today() - datetime.timedelta(days=100)).strftime('%d-%m-%Y')
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um alle Pakete zu löschen, die älter als 100 Tage sind
        delete_query = "DELETE FROM PackageData WHERE deliveryDate < %s"
        self.cursor.execute(delete_query, (thirty_days_ago,))
        self.conn.commit()
        self.cursor.close()



    # Holt den Lieferstatus eines Pakets basierend auf seiner Paketnummer.
    # :param package_number: Die Paketnummer des abzufragenden Pakets.
    # :return: Der Lieferstatus des Pakets.
    # 1 = eingetragen , 2 = gescannt , 3 = in Paketbox , 4 = Fehler
    #
    def get_delivery_status(self, package_number):
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um den Lieferstatus des Pakets zu erhalten
        query = "SELECT deliveryStatus FROM PackageData WHERE packageNumber = %s"
        self.cursor.execute(query, (package_number,))
        result = self.cursor.fetchone()
        self.cursor.close()

        # Überprüfen, ob ein Ergebnis vorhanden ist und entsprechend den Integer-Wert zurückgeben
        if result:
            # Systemlog schreiben
            self.log_event(f'Der Status des Pakets mit der Packetnummer: {(package_number,)} wurde abgefragt')
            return int(result[0])  # Konvertieren des Ergebnisses in Integer
        else:
            return None  # Kein Eintrag gefunden


    # Setzt den Lieferstatus eines Pakets und aktualisiert Lieferdatum und -zeit basierend auf der aktuellen Zeit.
    # :param package_number: Die Paketnummer des zu aktualisierenden Pakets.
    # :param status: Der zu setzende Lieferstatus.
    # :return: True, wenn der Status erfolgreich gesetzt wurde, sonst False.
    # 1 = eingetragen , 2 = gescannt , 3 = in Paketbox , 4 = Fehler
    #
    def write_delivery_status(self, package_number, status):

        status_int = int(status)

        if status_int not in [1, 2, 3, 4]:
            return False
        
        # Aktuelles Datum und Uhrzeit festlegen
        delivery_date = datetime.date.today().strftime('%d-%m-%Y')
        delivery_time = datetime.datetime.now().strftime('%H:%M')

        self.cursor = self.conn.cursor(buffered=True)
        # Lieferstatus, Datum und Uhrzeit in der Datenbank aktualisieren
        update_query = "UPDATE PackageData SET deliveryStatus = %s, deliveryDate = %s, deliveryTime = %s WHERE packageNumber = %s"
        self.cursor.execute(update_query, (status_int, delivery_date, delivery_time, package_number))
        self.conn.commit()
        self.cursor.close()

        # Aktuellen Zeiteinstellungsstatus abrufen
        current_status = self.get_delivery_status(package_number)

        # Systemlog schreiben
        self.log_event(f'Der Status des Pakets mit der Packetnummer: {package_number} wurde geändert')

        # Über Websocket verschicken
        self.websocketobject.send_packagestatus(status, package_number)

        if int(current_status) == 3 :
            emails = self.get_email_array()

            for mailadresse in emails:
                self.mailobject.send_Mail(6, (mailadresse))

        return current_status == status_int
    


# ==============================================================================================================
# ============== PAKETBOX ======================================================================================
# ==============================================================================================================
    


    # Holt den Status der Paketboxtür und gibt ihn zurück.
    # :return: Der aktuelle Status der Paketboxtür
    #
    def get_packageboxdoor_status(self):
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um den Status der Paketboxtür zu erhalten
        query = "SELECT packageboxDoorStatus FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()
        return result[0]
        


    # Trägt den gegebenen Status (1 oder 0) für die Paketboxtür in die Datenbank ein und überprüft, ob der Status korrekt gesetzt wurde.
    # :param status: Der zu setzende Status (1 oder 0).
    # :return: True, wenn der eingetragene Status mit dem Parameter übereinstimmt, sonst False.
    #
    def write_packageboxdoor_status(self, status):
        self.cursor = self.conn.cursor(buffered=True)
        # Status in die Datenbank eintragen
        update_query = "UPDATE Settings SET packageboxDoorStatus = %s"
        self.cursor.execute(update_query, (status,))
        self.conn.commit()
        self.cursor.close()

        # Aktuellen Status abrufen
        current_status = self.get_packageboxdoor_status()

        # Systemlog schreiben
        self.log_event('Die Packetbox wurde geöffnet.')


        # Checkt ob der Status welcher geschrieben wurde mit dem Inhalt übereinstimmt
        return current_status == status
 


    # Holt den Sperrstatus der Paketboxtür und gibt ihn zurück.
    # :return: Aktueller Status der Paketboxtür
    #
    def get_packageboxdoor_lock(self):
        self.cursor = self.conn.cursor(buffered=True)
        query = "SELECT packageboxDoorLock FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()
        return result[0]



    # Trägt den gegebenen Sperrstatus (1 oder 0) für die Paketboxtür in die Datenbank ein und überprüft, ob der Status korrekt gesetzt wurde.
    # :param status: Der zu setzende Sperrstatus (1 oder 0).
    # :return: True, wenn der eingetragene Sperrstatus mit dem Parameter übereinstimmt, sonst False.
    #
    def write_packageboxdoor_lock(self, status):
        self.cursor = self.conn.cursor(buffered=True)
        # Sperrstatus in die Datenbank eintragen
        update_query = "UPDATE Settings SET packageboxDoorLock = %s"
        self.cursor.execute(update_query, (status,))
        self.conn.commit()
        self.cursor.close()

        # Aktuellen Sperrstatus abrufen und überprüfen
        current_status = self.get_packageboxdoor_lock()

        # Systemlog schreiben
        self.log_event(f'Der Status des Paketboxschlosses wurde geändert')

        return current_status == status



    # Holt den Status der Paketbox und gibt ihn als Integer zurück.
    # :return: Integer-Wert des Paketboxstatus
    # 0 = inaktiv , 1 = aktiv , 2 = Errorbutton , 3 = Fehler
    #
    def get_packagebox_status(self):
        #self.db_lock.acquire()  # Explizites Anfordern des Locks
        #try:
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um den Status der Paketbox zu erhalten
        query = "SELECT packageboxStatus FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()

        # Überprüfen, ob ein Ergebnis vorhanden ist und entsprechend den Integer-Wert zurückgeben
        if result:
            return int(result[0])  # Konvertieren des Ergebnisses in Integer
        else:
            return None  # Kein Eintrag gefunden
        #finally:
         #   self.db_lock.release()  # Stellen Sie sicher, dass der Lock immer freigegeben wird



    # Trägt den gegebenen Status in die Datenbank ein und überprüft, ob der Eintrag erfolgreich war.
    # :param status: Der zu setzende Status (0, 1, 2 oder 3).
    # :return: True, wenn der Eintrag erfolgreich war, sonst False.
    # 0 = inaktiv , 1 = aktiv , 2 = Errorbutton , 3 = Fehler
    #
    def write_packagebox_status(self, status):
        # Konvertieren des Status in einen Integer und überprüfen, ob er gültig ist
        
        status_int = int(status)

        if status_int not in [0, 1, 2, 3]:
            return False

        self.cursor = self.conn.cursor(buffered=True)
        # Status in die Datenbank eintragen
        update_query = "UPDATE Settings SET packageboxStatus = %s"
        self.cursor.execute(update_query, (status_int,))
        self.conn.commit()
        self.cursor.close()

        # Systemlog schreiben
        self.log_event(f'Der Status des Paketboxs wurde geändert')

        # Über Websocket verschicken
        self.websocketobject.send_boxstatus(status_int)

        # Überprüfen, ob der eingetragene Status mit dem Parameter übereinstimmt
        return self.get_packagebox_status() == status_int
    


# ==============================================================================================================
# ============== EINSTELLUNGEN =================================================================================
# ==============================================================================================================
    


    # Holt die Betriebszeiten und gibt sie im JSON-Format zurück.
    # :return: JSON-String mit Start- und Endzeiten.
    #
    def get_operating_time(self):
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um Start- und Endzeiten zu erhalten
        query = "SELECT startTime, endTime FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()

        # Überprüfen, ob ein Ergebnis vorhanden ist
        if result:
            # Ergebnis in das gewünschte JSON-Format umwandeln
            operating_time = [{
                "start": result[0],
                "end": result[1]
                }]
            operating_time_json = json.dumps(operating_time)
            return operating_time_json
        else:
            return False
    


    # Fügt Betriebszeiten in die Settings-Tabelle ein oder aktualisiert sie.
    # :param start: Startzeit.
    # :param end: Endzeit.
    #
    def write_operating_time(self, start, end):
        # Überprüfen, ob Start- und Endzeit vorhanden sind
        if (start != "") and (end == ""):
            self.cursor = self.conn.cursor(buffered=True)
            # Betriebszeiten in Settings einfügen oder aktualisieren (Prepared Statement)
            query = "UPDATE Settings SET startTime = %s"
            self.cursor.execute(query, (start,))
            self.conn.commit()
            self.cursor.close()
            # Systemlog schreiben
            self.log_event('Die Startzeit wurde geändert')
            return True
        elif (end != "") and (start == ""):
            self.cursor = self.conn.cursor(buffered=True)
            # Betriebszeiten in Settings einfügen oder aktualisieren (Prepared Statement)
            query = "UPDATE Settings SET endTime = %s"
            self.cursor.execute(query, (end,))
            self.conn.commit()
            self.cursor.close()
            # Systemlog schreiben
            self.log_event('Die Endzeit wurde geändert')
            return True
        elif ((start and end) != ""):
            # Betriebszeiten in Settings einfügen oder aktualisieren (Prepared Statement)
            self.cursor = self.conn.cursor(buffered=True)
            query = "UPDATE Settings SET startTime = %s, endTime = %s"
            self.cursor.execute(query, (start, end))
            self.conn.commit()
            self.cursor.close()
            # Systemlog schreiben
            self.log_event('Die Startzeit und Endzeit wurde geändert')
            return True
        return False



    # Überprüft ob die Aktuelle Zeit im Betriebszeitfenster liegt
    # :return: True, wenn die aktuelle Zeit im Betriebszeitfenster liegt, sonst False
    #
    def check_operating_time(self):
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um Start- und Endzeiten zu erhalten
        query = "SELECT startTime, endTime FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()

        if result:
            
            # Aktuelle Uhrzeit im Format HH:MM
            current_time = datetime.datetime.now().strftime("%H:%M")
            
            # Vergleich, ob die aktuelle Uhrzeit innerhalb des Zeitfensters liegt
            if result[0] <= current_time <= result[1]:
                return True
            else:
                return False
        else:
            # Wenn keine Zeiten in der Datenbank gefunden wurden
            return True



    # Holt den Status der Lichtfunktion und gibt ihn zurück.
    # :return: der aktuelle Status des Lichtes
    #
    def get_light_status(self):
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um den Status der Lichtfunktion zu erhalten
        query = "SELECT light FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()
        return result[0]
    


    # Trägt den gegebenen Status (1 oder 0) für die Lichtfunktion in die Datenbank ein und überprüft, ob der Status korrekt gesetzt wurde.
    # :param status: Der zu setzende Status (1 oder 0).
    # :return: True, wenn der eingetragene Status mit dem Parameter übereinstimmt, sonst False.
    #
    def write_light_status(self, light_status):
        self.cursor = self.conn.cursor(buffered=True)
        # Status in die Datenbank eintragen
        update_query = "UPDATE Settings SET light = %s"
        self.cursor.execute(update_query, (light_status,))
        self.conn.commit()
        self.cursor.close()

        # Aktuellen Lichtstatus abrufen
        current_status = self.get_light_status()

        if(current_status == 1):
            # Systemlog schreiben
            self.log_event('Das Gartenlicht wurde aktiviert')

        # Checkt ob der Status welcher geschrieben wurde mit dem Inhalt übereinstimmt
        return current_status == light_status
    


    # Holt den Status der Türklingel und gibt ihnurück.
    # :return: der aktuelle Status der Klingel
    #
    def get_bell_status(self):
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um den Status der Türklingel zu erhalten
        query = "SELECT bell FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()
        return result[0]
    


    # Trägt den gegebenen Status (1 oder 0) für die Türklingel in die Datenbank ein und überprüft, ob der Status korrekt gesetzt wurde.
    # :param status: Der zu setzende Status (1 oder 0).
    # :return: True, wenn der eingetragene Status mit dem Parameter übereinstimmt, sonst False.
    #
    def write_bell_status(self, status):
        self.cursor = self.conn.cursor(buffered=True)
        # Status in die Datenbank eintragen
        update_query = "UPDATE Settings SET bell = %s"
        self.cursor.execute(update_query, (status,))
        self.conn.commit()
        self.cursor.close()

        # Aktuellen Türklingelstatus abrufen
        current_status = self.get_bell_status()

        if(current_status == 1):
            # Systemlog schreiben
            self.log_event('Es hat geklingelt *dingdong*')

        # Checkt ob der Status welcher geschrieben wurde mit dem Inhalt übereinstimmt
        return current_status == status
    


    # Holt den Status des Fehlerknopfes und gibt ihn zurück.
    # :return: Aktueller Errorbuttonstatus
    #
    def get_error_button_status(self):
        self.cursor = self.conn.cursor(buffered=True)
        query = "SELECT errorButton FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()
        return result[0]



    # Trägt den gegebenen Status (1 oder 0) für den Fehlerknopf in die Datenbank ein und überprüft, ob der Status korrekt gesetzt wurde.
    # :param status: Der zu setzende Status (1 oder 0).
    # :return: True, wenn der eingetragene Status mit dem Parameter übereinstimmt, sonst False.
    #
    def write_error_button_status(self, status):
        self.cursor = self.conn.cursor(buffered=True)
        update_query = "UPDATE Settings SET errorButton = %s"
        self.cursor.execute(update_query, (status,))
        self.conn.commit()
        self.cursor.close()
        current_status = self.get_error_button_status() 

        if(int(current_status) == 1):
            # Systemlog schreiben
            self.log_event('Die Fehler Taste wurde gedrückt')
            # Paketnummer existiert nicht oder Status ist nicht 1, E-Mail senden

            emails = self.get_email_array()

            for mailadresse in emails:
                self.mailobject.send_Mail(5, (mailadresse))
        
        return current_status == status 



    # Holt den Status der Tür und gibt ihn zurück.
    # :return: True, wenn der Türstatus 1 ist, sonst False.
    #
    def get_door_status(self):
        self.cursor = self.conn.cursor(buffered=True)
        query = "SELECT doorStatus FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()
        return result[0]


    # Trägt die aktuelle Temperatur in die Datenbank ein und überprüft, ob der sie korrekt gesetzt wurde.
    # :param temperatur: aktuelle temperatur an der Klingel
    # :return: True, wenn der eingetragene Status mit dem Parameter übereinstimmt, sonst False.
    #
    def write_temperature(self, temperature):
        self.cursor = self.conn.cursor(buffered=True)
        update_query = "UPDATE Settings SET temperature = %s"
        self.cursor.execute(update_query, (temperature,))
        self.conn.commit()
        self.cursor.close()
        current_temperature = self.get_temperature()
        
        return current_temperature == temperature 

    

    # Holt die Temperatur und gibt sie zurück.
    # :return: aktuelle Temperatur.
    #
    def get_temperature(self):
        self.cursor = self.conn.cursor(buffered=True)
        query = "SELECT temperature FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()#
        self.cursor.close()
        return result[0]


    # Trägt den gegebenen Status (1 oder 0) für die Tür in die Datenbank ein und überprüft, ob der Status korrekt gesetzt wurde.
    # :param status: Der zu setzende Status (1 oder 0).
    # :return: True, wenn der eingetragene Status mit dem Parameter übereinstimmt, sonst False.
    #
    def write_door_status(self, status):
        self.cursor = self.conn.cursor(buffered=True)
        update_query = "UPDATE Settings SET doorStatus = %s"
        self.cursor.execute(update_query, (status,))
        self.conn.commit()
        self.cursor.close()
        current_status = self.get_door_status()
    
        if(current_status == 1):
            # Systemlog schreiben
            self.log_event('Die Gartentür wurde geöffnet.')
        
        return current_status == status 

    # Holt den Status der Zeiteinstellung und gibt ihn zurück.
    # :return: der aktuelle Status der Zeiteinstellungen
    #
    def get_time_status(self):
        self.cursor = self.conn.cursor(buffered=True)
        # SQL-Abfrage, um den Status der Zeiteinstellung zu erhalten
        query = "SELECT timesStatus FROM Settings"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        self.cursor.close()
        return result[0]



    # Trägt den gegebenen Status (1 oder 0) für die Zeiteinstellung in die Datenbank ein und überprüft, ob der Status korrekt gesetzt wurde.
    # :param status: Der zu setzende Status (1 oder 0).
    # :return: True, wenn der eingetragene Status mit dem Parameter übereinstimmt, sonst False.
    #
    def write_time_status(self, status):
        # Status in die Datenbank eintragen
        self.cursor = self.conn.cursor(buffered=True)
        update_query = "UPDATE Settings SET timesStatus = %s"
        self.cursor.execute(update_query, (status,))
        self.conn.commit()
        self.cursor.close()
    
        current_status = self.get_time_status()
    
        if(current_status == 1):
            # Systemlog schreiben
            self.log_event('Die Betriebszeiten wurden aktiviert')
        elif(current_status == 0):
            # Systemlog schreiben
            self.log_event('Die Betriebszeiten wurden deaktiviert')
        
        return current_status == status 



# ==============================================================================================================
# ============== VALIDIEREN ====================================================================================
# ==============================================================================================================
 


    # Validiert ob die Nutzerdaten korrekt sind
    # :param user_data: Die User Daten im JSON Format
    # :return: True, wenn die Daten korrekt sind, False wenn nicht
    #
    def validate_userdata(self, user_data):
        if (user_data["firstname"] and user_data["lastname"] and user_data["address"] and user_data["city"] and user_data["address"] and user_data["city"] and user_data["zip"] and user_data["email"]):
            return True
        else:
            return False