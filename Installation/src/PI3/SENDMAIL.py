import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
#from Database import Database
from dotenv import load_dotenv
import os
#Diese Klasse Versendet E-Mails
# Es muss der Grund übergeben werden 1-6 und die Empfänger Mail 



load_dotenv()


    
# Als vorlage für diesen Code diente eine interne E-Mail
html_head = """\
<html>
<head>
<style type="text/css">
    /* RESET STYLES */
    img {
        border: 0;
        height: auto;
        line-height: 100%;
        outline: none;
        text-decoration: none;
        max-width: 540px !important;
        margin: 0 !important;
    }
    figure {
        border: 0;
        height: auto;
        line-height: 100%;
        outline: none;
        text-decoration: none;
        max-width: 540px !important;
        margin: 0 !important;
    }
    table {
        border-collapse: collapse !important;
    }
    body {
        height: 100% !important;
        margin: 0 !important;
        padding: 0 !important;
        width: 100% !important;
    }
    p {
        font-size: 18px;
        font-weight: 400;
        min-height: 25px;
        line-height: 25px;
        margin: 0;
        display: block;
        clear: both;
    }
    ul,
    ol {
        margin-top: 9px;
        margin-bottom: 9px;
    }
    .signature {
        font-size: 16px;
    }
    .small-text {
        font-size: 10px !important;
    }
    .company,
    .heading,
    .environment {
        font-weight: bold;
    }
    .author {
        color: darkblue;
        font-weight: bold;
    }
    .environment {
        color: green;
    }
    .line {
        margin-top: 20px;
        border-top: 1px solid #000;
    }
</style>
</head>
<body>
    <table border="0" cellspacing="0" cellpadding="0" width="100%"
        style="cellpadding:0;border:0;cellspacing:0;display:table;width:100%;table-layout:fixed;border-collapse:seperate;float:none;"
        align="left">
    </table>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <!-- DHBW LOGO -->
            <tbody>
            <tr>
                <td bgcolor="#324376" align="center">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAAAoCAMAAAA2TQ8LAAABOFBMVEVHcExdaXFdaXFfa3NdaXFdaXFdaXFeanLiABpdaXFdaXFdaXFdaXFdaXHiABpdaXFdaXFsYWriABpdaXFdaXFdaXF9iZBdaXHiABpdaXHiABpdaXFdaXF9iZBdaXHiABriABrjABniABqSERh9iZB9iZBdaXHgBR1daXHiABqPDxVdaXFdaXHiABrgABmODxbiABriABpdaXFdaXHiABpdaXFdaXF8jJJdaXFdaXFdaXHiABqDZWzjABp8i5J9iZDiABp9iZCNHSNdaXHiABriABriABriABriABriABriABp9iZB9iI98i5LiABriABqPXmiMJCqEb3aAe4KBdHuvDRuaFR63CBiNHSOQEBaMKjHiABp9DxriABp9iZCODxXdARqCcHeNFhy2CBibDRaxCReNHSNHcExoMkajAAAAaHRSTlMAVyEJKxVRBfI5QiZMHJeJ+wM4DlxG4WHePg8y7yBwB/ot7Rgt7ZQ134OxaujO1dQUpMNluLytF9DXfCf+bIfZkET+oOkeUEdhxXO+l0EcVaz+3v67+fyv1NSxWf//////////////AMNbhGEAAARcSURBVEjHzZULV9s2FMclWZIjWSYxtusYG5PQzU0Wl5W8CGGsbAwGbdfu/Txuabvt+3+EXdkBkpLHYWc5Z/+TE9nKzf9n33slITRfO+0mWqd22rub6/VvVDbX65+vE6D91wko/NcIKP3XCDgu/P87wLONGR3v/PJXoZ++fVHoc6OMe9I+AbXPn5SveQyR+uL8pA0zfgqqBjZMqKoF32ZnMoxRPZ/W1d7DT94Uevf2daEH2yVgs1IENB5faOOtx/legdrPK48QqtZG3e4oPlQIJbUApu1WrVcMo1nA1fvflgD2Njb6lbzRduYApGm4vNWykKxVYTqMaykMUZzOAK7ev/l0CWADUnNezyvncwBC3yTw2CQbmgj1Wt0B5BbDRH3GfxUAoVeVvL91FxDqG15LkNE9ZAilg3ELKjKOxRRA+68G7PR1g2nA1p03SGOpvygyBqmKQ+QMAVaf8V8NQO08v9CA+quvQf3bGrAkq0J2OjWF7MOelfWQO4J01Wf8bwHf/bAAcJznzzQgb2jl11006B7GY/CHKidIZKE7SqEeUPBrQOl/A/jys9crAI2vtBoTwDBNT0ddDr+T7BQFUIDhwAxjfAO4+nMGcOO/JEWbR6D+VA3IMIaFYHS7RrVroHGLBXG0AHDrv6TId7uIxrpF0xYZnEJLxaKqG2oeYMr/Q4CzuZtfLmpTdjhyocqxGiVAy3pDzZsDmPb/EPCyn+fHaNEbZHp1hXHagnZ1R6cjvajvAv6e9p8G7B81/7jM8/2tBSvZTaGB9AbUahF412Gma3wX8PuM/zSgsbsLkf2zeZvdmPPeoHYKGYJVVuvqLXhcgyWH0OVuqZ/fFfr14YuPZ/T8oAQ06zqq3r8oFvBWv9IvLk4qe0d6S8iyrNXtuEVoNdapQSob6PuzZqFH378t9KNx8NGMtp3JQXekw852JsfIy+ZZeUw0jyCAURCZnBxwx/RgUHvq3Dl4Wibkm3UdlCXgwRdorYBl/gZh5YVDiqS5plGmxJ3Mm2VCbGMxYOnz0x6mruEyy/AMGzx9IqhrOS6h2HZsZlp2YBkQgBmzFwCW5yeSdsCpJIHAVhC4iHuJ4FhwEXW8KLBDRRIccktxGgRsLmBFfSNpeZiqsKe47PgAkNjHxPMQlX6kpMcM+BVbnIeJPxfwfEV9IUU2D5QXhDzyIhOFtqDKo9yPhKASh9jyLaE8pQQW5pz/m9srusBkBjKY6cLH0HU1HdMxGEwZJhxmhgOjAxdQepeh/6VsLIsGFNfPR2xYvIRSB5nEJM51Jq1/C4iwR4V0/YRKYXPfRFQFnhI0IjyqRlUifKa4EmESuiqylMWVeU/AuEdooiTHvcCHZvRJD3ueCJXEbsACKwl8rDtLci8IeOgrz7kngHMPJwpcPEsIRVwzUbgDjSR9M3A9q0OpkhEAMMbEpzS5b64YD21fMukzGdk2gY04sgk1fN8mumWJkIwQZkHCXJ8SFnrcXGtPSA+XgH8Auh05j7LbcjQAAAAASUVORK5CYII="
                                    role="presentation" width="96" height="auto" style="max-width: 540px;" border="0">
                            </img></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td bgcolor="#324376" align="center" style="padding: 0px 10px 0px 10px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                        <tbody>
                        <tr>
                            <td bgcolor="#ffffff" align="center" valign="top"
                                style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #0D0D0D; font-family: 'IBM Plex', Helvetica, Arial, sans-serif; font-size: 24px; font-weight: 600; line-height: 32px; box-shadow: 0px -4px 4px rgba(0,0,0,0.08)">
                                <h1 style="font-size: 24px; font-weight: 600; margin: 0;">SecBox Information
                                </h1>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td bgcolor="#EDEDED" align="center" style="padding: 0px 10px 0px 10px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%"
                        style="max-width: 600px; box-shadow: 0px 4px 6px rgba(0,0,0,0.08); margin-bottom: 16px">
                        <tbody>
                        <tr>
                            <td bgcolor="#ffffff" align="left"
                                style="padding: 10px 30px 40px 30px; color: #666666; font-family: 'IBM Plex', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; border-radius: 0px 0px 4px 4px;">
"""
# Fall 1 : Welcome Message (erster benutzer)
html_message_1 = """\
                                <p>Liebe Kundin, lieber Kunde, </p>
                                <p><br>
                                </p>
                                <p><strong>🥳🎉 Wir freuen uns, Sie bei SecBox willkommen zu heißen 🎊🍾</strong></p>
                                </p>
                                <p>Vielen Dank, dass Sie unseren Service gewählt haben. Wir setzen uns dafür ein, Ihnen ein außergewöhnliches Erlebnis zu bieten und sind zuversichtlich, dass Sie von den vielen Vorteilen profitieren werden, die wir bieten.</p>
                                <br>
                                <p>Wir freuen uns, Sie an Bord zu haben und freuen uns darauf, Sie auf Ihrer Reise mit Ihrer Paketbox zu begleiten. Entdecken Sie die Möglichkeiten und erleben Sie den Unterschied mit uns!</p>
                                <p><br>
                                <p><br>
"""
# Fall 2 : Registration Message (weitere benutzer)
html_message_2 = """\
                                <p>Liebe Kundin, lieber Kunde, </p>
                                <p><br>
                                </p>
                                <p>Wir freuen uns, bestätigen zu können, dass Ihre E-Mail-Adresse erfolgreich bei SecBox registriert wurde. Vielen Dank, dass Sie diesen Schritt unternommen haben, um mit uns in Verbindung zu bleiben.</p>
                                <br>
                                <p><strong>Was kommt als Nächstes?</strong></p>
                                <br>
                                <p><strong>Bleiben Sie informiert:</strong> Sie erhalten nun Updates und wichtige Benachrichtigungen direkt in Ihren Posteingang.</p>
                                <br>
                                <p>Wir setzen uns dafür ein, Sie über die neuesten Entwicklungen bei SecBox informiert und verbunden zu halten. Wenn Sie Fragen haben oder Unterstützung benötigen, steht Ihnen unser Kundenservice-Team jederzeit zur Verfügung.</p>
                                <br>
                                <p>Vielen Dank, dass Sie unserer Gemeinschaft beigetreten sind, und wir freuen uns darauf, mit Ihnen in Kontakt zu treten.</p>
                                <p><br>
                                <p><br>
"""
# Fall 3: The scanner detected an unexpected package
html_message_3 = """\
                                <p>Liebe Kundin, lieber Kunde, </p>
                                <p><br>
                                </p>
                                <p>Wir haben eine wichtige Nachricht für Sie. </p>
                                </p>
                                <p><strong>Wir möchten Sie darüber informieren, dass ein unerwartetes Paket an Ihrer Paketbox gescannt wurde!</strong></p>
                                <p><br>
                                </p>
                                <p>Dies könnte auf eine <strong>Fehllieferung</strong> oder eine <strong>nicht registrierte</strong> Sendung hinweisen.</p>
                                <p>Wir bitten Sie, diesen Sachverhalt zu überprüfen und geeignete Maßnahmen zu ergreifen, um die Sicherheit und Genauigkeit der Lieferungen zu gewährleisten.
                                <br><br>
                                <p>Bitte <strong>überprüfen Sie die Situation umgehend</strong> und stellen Sie sicher, dass alle Sendungen korrekt registriert und zugewiesen sind. Ihre schnelle Reaktion ist entscheidend, um Unregelmäßigkeiten zu vermeiden.</p>
                                <p><br>
                                <p><br>
"""
# Fall 4: The package box has been open for more than 3 minutes
html_message_4 = """\
                                <p>Liebe Kundin, lieber Kunde, </p>
                                <p><br>
                                </p>
                                <p><strong>Wir haben festgestellt, dass Ihre Paketbox seit mehr als 3 Minuten geöffnet ist!</strong></p>
                                <p><br>
                                </p>
                                <p>Dies könnte auf ein <strong>Sicherheitsrisiko oder einen Defekt</strong> hinweisen.</p>
                                <p>Bitte überprüfen Sie umgehend die Situation und ergreifen Sie die notwendigen Maßnahmen, um Sicherheit und reibungslosen Betrieb zu gewährleisten.</p>
                                <br>
                                <p>Vielen Dank für Ihre prompte Antwort.</p>
                                <p><br>
                                <p><br>
"""
# Fall 5: Error Button was pressed
html_message_5 = """\
                                <p>Liebe Kundin, lieber Kunde, </p>
                                <p><br>
                                </p>
                                <p>Wir haben eine wichtige Nachricht für Sie. </p>
                                </p>
                                <p><strong>Wir möchten Sie darüber informieren, dass der Fehlerknopf an Ihrer Paketbox gedrückt wurde!</strong></p>
                                <p><br>
                                </p>
                                <p>Bitte informieren Sie uns umgehend über den aktuellen Status, damit wir angemessene Unterstützung leisten oder notwendige Maßnahmen ergreifen können.</p>
                                <br>
                                <p>Wir schätzen Ihr schnelles Handeln in dieser Situation und stehen bereit, um jegliche benötigte Unterstützung anzubieten.</p>
                                <br>
                                <p>Sollten Sie Fragen haben oder weitere Anweisungen benötigen, stehen wir Ihnen jederzeit zur Verfügung.</p>
                                <p><br>
                                <p><br>
"""
# Fall 6: A package was successfully delivered
html_message_6 = """\
                                <p>Liebe Kundin, lieber Kunde, </p>
                                <p><br></p>
                                <p><strong>Wir freuen uns, Ihnen mitteilen zu können, dass Ihr Paket erfolgreich zugestellt wurde.</strong></p>
                                <p><br></p>
                                <p>Wir hoffen, dass alles zu Ihrer Zufriedenheit verlaufen ist, und danken Ihnen für Ihr Vertrauen in unseren Service.</p>
                                <p><br></p>
                                <p>Für Rückmeldungen oder Fragen stehen wir Ihnen jederzeit zur Verfügung.</p>
                                <p><br></p>
                                <p><br></p>
"""
html_signature = """\
                                <div class="signature">
                                    <p><em>mit freundlichen Grüßen</em></p>
                                    <p><br>
                                    <p><br>
                                    <p class="author">i. A. Walter Frosch</p>
                                    <p>Head of E-Mail Versending</p>
                                    <p><br>
                                    <!-- SecBox LOGO -->
                                    <img
                                        src=" data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFwAAABcCAYAAADj79JYAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5wwNCi4ZkvSQfAAALOdJREFUeNrtfGeUVVW27jfX2idWzgEoCqhAhpIsSUBEbERtzCLG1ja093YO2rRtt/26bcO1r6G1g5i6FROKoKKSU1FQUImiElVFJSrnE3ZY8/3Y5xTo6zcG9Lvcvm+MmgwGxamqfdb+1lxzfvObcx9g2IZt2IZt2IZt2IZt2IZt2IZt2IZt2IZt2IZt2IZt2P4FRv/qBZyL1Q4oZEYQANCO/ONob2vDhNzJOHKsmhlB3H7dIggh/tXLPCf7Hwe4UgqL/q0QLw/OhOfpIG1+fx/SkiJgQtH2L3aJD957Bx2NVTx3+VV01ZWreGJOlqqsqkXOxBxcMnsKoiPBpTVdmDwuHkT/427vXws4M6Oitw+5MdHoBWjv1kK0+9sRE5FGNTW1tHnLdtr9+acMs1IB4NB6+cwVxhFwmgElkkcvEFesvgyXXJynpFRc3tiEh265ASnJHv7jxt2459qFkPJffwr+WwEPGozv/uItpE68EdcuGKQP396MyXkT0NndSfsOFtAnWz6nxsovFQAFpJJSzSAiBuAoO9mU+Mqr701rbm6aL4WIc7pcJ2bPmrH3nluvrI0h6pt861N4+v6LMW/ePJYAWSJVXLzsSlq1agmPzRnL3Z39HBPlwTWr58MNcHVzH7LSo//bT8EFezdmxsnmLoxNj0evbtHuXSWorT+BEWlZqKg+JT7Zuov2797OKlCqAPD27Ttp6dLfgflm9PXfFPvuRzsy8guLZw0Mquk+f2Dm4MBAdiAQiAsGg2RYJlwuF5wOVzDC42qPjvYei/C6i1JSYgtWLJldsnje1EYQ9PbTXZSSPgHMbQxAJKTPo8tXXoFLF8/j9LRE3rGvAHesXYOcsUEGpcIwLDid2v8fgNd2M07lV2LRihzUntJpyyfbkZoSj57BQfpi+x76bNt26mlqVGC3krExFGjfBU3TGIBjV37xiC2f7ZrQ3tY1M6BjVm9vYKrfb6YGDdMVCBKYJQACkcZSapbFipSyoJQpiQhSKGhCwe0ScDm42+0R9dFR3sORHvfRyVNyCy+/4pLq7LT4TgBcUVlJ43Nz7VMgR4tp8y+h1Vcs4ylTJnNlZRVPGZeD1VdOBwBu7zCRnOT4nwF4cUkbpkxKwunTQdq+vxAdpxswOmMMTlTUis+2HaA9e79g02977y233EKr13wf0ydP4NhYjvngkz2ZZaXVMzo6uqYP+s2ZAT040R/wx+imiaCpwJBgCBCRkuRiMJGyzz4xA+EowMQABIPBYGYoJYgVCbJAsOCQBE+EMJ0O6oz0RhyNinIdTE2IK1y+aH750mXTGgAEzQGLEkbkoq+vhgFQQtoCcdXVV9CcOdNUZsYozj94DCtWLsfs6akAwMGggtstLzzgxfX12PDc23jqiR/S93/6JKbNmIigf4Dy8yvp4w93oLXxEAMuFRExi1rb3keE18MAHFu3F47cvuvAhO7O7rw+X3DmwIA+JRDASH/AcpmmgqUkSAgwgRWxBWIhCASAwAqCGSABDr0EACGEAWIQA4AAQYBYgEiyUsxEYFYKRJZk1iEIIFiIivDAoXGf2yXro6OjD0VFuwqys0YXfeOK5dVvbjrWUVl6iL5100IsWzKPHQAZnqli/LQcrL5sIebPmckxIxJ48bRc3na0iBKd8TxjcsaFAbyX+eu/wwAQDdArr36Oi6bncmpqdOwHH+zKLCo9OdnvN+a0d3Yt8ZvIMJmjAnoQlgIsFmBIQApFQjIp0/Zc2NmRSIAUQBAQIDDroXc8e6kMJvtfYoBA9mKYQAQwFBjK/pqdADQGwAxiy9IFk0lCMAgWPA4NXk0zAbMrwus6npQQtSM2xnV48SWzq1avmF8PQB8cDFBEhJsPFxX9QwxmTZ/+Xw/4r3//MkVGRrAmiZrauh09va1a9phRac0NgzkDg+bcnp7BvP4BfUpQ53R/UHeayoTJFkylQQgNDFhEREQgZkUhvMBkhQCzvdQOGfYrzAxFhBBy/3D1xDbVY1IgKBtWIgACYAKUfb3QmQAIYGK20zozKxApEsQMSQSnYLicBJcTPR6XVh0VF1PgidBKkuM9ZRkj0uvbB3s79u3MDzz/7p1q+yuRONH6KX7/0+/yueJ4zoAzM23428cYlT6ChaCkLds+u7mxuemKnj5tweAge4M6I6gTQBqEQ5osWQnBQrFp37kiwhCYhJCLguAA+Ov8WAFkhTyZvvr6/2Ei5Ggq5PHh0EP2dckKfU+EXhdg2KeMiGCRDpBiYqkES8UmA2w6SBkQwoLmUHA6AoiPcXckxSfsz8ub9uID3/rmpwDo+IlTmDg+45zBPi/AX37tfWHoQaVp2oiG5u68JcuurBo9Qjv96d7i1KKS8lnNTadnmzou9g9auUG/EalYwDQBkxWE1AAiBYCJmOyAwAQGBCsQMRgEYgrF6pD7n4FuqOrBkK+G71NDOKjYV1FgsiBCm2BR6JosQKE/gGBmYvs0MSlWJISCJIYUgMMhLZfL1eB2u4tiop37p00fWzg5N70sNW2Ce+vmT3N7+xrifv/LH79ddHKH2rfvND2w7uZzBv2cSWd55UkhiZXU5Iyi0lMf7s6vbPd6tGNxsRGHo7zuwutXLH5p3Y2X/mr3sVORO7cXTK6prJnU19e3aCCIqYP+YLqylLAYsCy2vZskA0KRYAJZNOScCHt+CFLWhryYGfbGkJ0w7TCjAJwBlUkASoJDnk2ssYBkMDOzIgYLUhZJAgkh4BAWPC7Z7XK7y70RziOjM0cdmTI9t2zl8nmtLbV1rlc27hlTWtw488Du4od6ezZN0fVgZkycqADw9rSxE/DqK0fPx8HPHXDddEDTGMxasG9AYcBvJPUN6Ms7T1vLXU4fKsq3Wa+/ubUhKtZ5IC01dv/EqWm7v33nd/5GDgQ/3Lx3RNGxE1Nrahvn6kE1JxhUY4MBPdowWRqWB8wuCMFgWAoAC8Gk2BySo5gZCgQWIpQYGYBlf08EAVYgctibww4mSAYTFCuSzETKJE0DSAMcDlhul6PO5XaVJCYkHszJSi2eMzWzfMmSWdYb729LOFZYOXHblp23vvHXjxbqupVtsYwMmgbADMt0MZMLboMHiUgxMzp7AxcGcEkOlgRIkorICZBgFkqZIBimBZiWBKzMLr8vs7HVd1NhaRu2fFreG+U2y+KiIva5PFEly+YveunBe77xs72Hy1yHDldMLS4qm9rVzwt8vuDsQMCfahiWABMsU9jeysSkKSZBEJBksiIiCcWAIDsTCOXmUDqEUpYA6yQlCGA4hYDHKfojva4Sh+Y4NG5cZtH0i7JL1l49v+VQWXnMm29tmlBee3Lm7iPF9/7yD3+bYVlWumEooeuAgAeW5YAQxJKgQCDpcLLFumToVhiXYMB/YQDnoeQkFFjaZxtKstDtYwwBsJNNCDYZUAYL6FZMYNBxcWeb/2KSPlTVnFbvfPhFfVSU50j6yJSC+NTE/Md/e/1Gy2D1wSd7k05WNU5sbmyf6/eZC/SAlRXwByNMZZFpKVgsIYQGgJVNwZWAUkRwkkYE6TDgcCt2ukWTyy1LoqO9u3Jzs4sWLpxUsyRvenDDe9tiDx2qmL5l68HbXtvwxVxdtyYZhunV2YIBBrMGkMMmTkSKhSCWTKyIFEtpYyCVYgHFNmxERGvuWH9hACcKlx5nEXImaMoBgu11DEEQIMUWpABYKDaZmUkwEeDXDSmAMT49MKa1o/Zap1PDrTf/dsClyeOJibGH4uIjj65YdvFf7r5lxW93Hy5xHC44nl15onFCa3vffH/QnOHz65mmZQgFhoMknE5Xv9slKz1ux56x47IKx41LOXHz9ctb+npMxwt/fDen/kTLRUfyq+799eDfZlmmlRbUlbAsglISSglI4VQkiSVbRCQIAClmIoKECjMiE0xmyOkYLAwQWTarZeZr7/rZeVXr56HUhJgBCSZiEGlgFuAQ/nbBYf8MCbv4sLfJICKbkgmhgQA2mVmQYF9ACZ+yIiUZszv7W2Zrp4DispPWW29/0eB2uYuSEpIPe6JE4eO/vXfTxIx0/6sb9yeUlRyZ3dndOnJC7rSCJZfMq54xZaTvfz33RvqJ8sbpTU1d697/4ODsgG6MN00VbZgWdItBQoKZIIRTETFDI5IAgYPCXqUAsxmiRGEqSqFblpAgABZMASgoAGrI6QTOixWeh4ezYYPJTCATgACIoYQKMQr+SkVIEPb+hBc+tGlEgoiYFYQksKaxxRpbSrFiUDCgJNjI1AatzPbOk1cJh4n7H3y6V5OyMj4qpmxkatS7f3nu139/4Ad/+O6O3zx/v9/nm2qYanQwaAmQC5aSUIoAKRkkFWlDpD9MzENOQgiVtjZ8RAApnCmRQsVTiC5R+P8QkGcVYZLPT2M/9xhONgVjsuVqRVaIWquvgMxAmN997XfPAj0sQDGgWBAEEUkJixUgBQgaG2AmgJXhEn4DMRLmjN6e9lkDgz25f/9gR/PxEw2/6vWZDjt/OEAkLVZETAThAFlsEcAyXGSJId0lVInyUO11Fs0M830xVKEwFEDmkNMQf1W4+vr//8sAtw8PASElz97tUA0XciAO+UEYV8JZN8X0lTKLQ1cTRFDK/uGw4zCImEIVuhQABCtiRZYlTOI+djv9FsmgReQgIe1dYrJPPisopSCIQqX/2ehQyCHCTkAIV6823hwKjaH1MoMFoNjWdcDS3gBWZ93J+Qmu5x5S4LRPHWmKWGNSEoKEHWJAQx5riyEMSSokIonQNpypFYd2ghjEFkIUIBSWhi4EMIeOuyJiUwAGCBASAQIrYbM1COIzl7e9l74aykLvF95zDolePPQDZ2u+wuZjHBK/oIFBYGIwhC1lamfCiMnmhQE85CAgQLAyiEwLUkgAUHaWFGfukgDFIf0v5LlhsSoMAIV8nKCGvNDWqM4CAQQJAWZhy1vMkOwQgp0koJFgC8QhrYTOhOozYIeBsgGlcJELDCVGJmWL7ExDoDOrkJwLtjORgoApGEooFQAxxzCzRKDYSk2OujCAx0a72OPWIKVoTkqI/MLlDI4P6oMphmKHZRIUJFhJECQTSbYLFKKQWoUzEGMIjHDk539wKm1dBaGYwCEl0QGlnGClASwRzh9DiS4UJr6qwoRzis0+iPksCSwcyAVCUYwBxWAWzIoUmyQ1kyQZcEhGpDey1e2NahiZHPEREVkA6NW/bb4wgF9z1Rw2DQOKrcYrvnH5g4MD/uD2XZ8nldU0LGpr7Z+jB3mqqSPTMMhlGCYhVBGSHZ+VHXYIsLsFCGsjaugoh8NBaFMoDJJhq3uhEARiocgEC5OYdIBkSG8JtSVCG8QhrxcsIJR9AHFWu4jDXSICWLEAK5ICpGkCTAyP22U6XVE1bgcfT010HxqfO3bnJUsXtA0GjOiTpUWieH0rfnl6GrebwQvDwze+v1toQlkOhyNvf8Hb7/h1d11cQszujOS0whVrlv1m9pypAwcLSpzlpVWT6k81Tu7rG5wR9AenBwwaoZtKKKVgmAwhJEBSgYiZQSSZLFYkhQa2wlo228xAWSCW9l8CABMEE2ATxMoGk+2+m2IGRJh92PFAhTZASVu6EiSYFROzEvZPMTmI4XIKuF3Odo9HK/NGuY9mjhlxZML4cSfyLho7UFTeEHP44Mlx+Ufrb/hs34lZgz7/zAS3VXXw7WumAi38g58+eWE83O8z4dAYlkmyvz+YMuBXKT19g3Na6hiFhRW+v77+XpPDidKUlPgDY3NS912+bNHf0hOS9Y+2Hkgtr66+qKOja9aAT78oGDDH6rqKMEwFoQQsHdBIgkAKQjCzDRfbsqAdXxGO6AwmxSRluAqDotDIigiFHrJFBmbFksBQLsFKEjGTFARNMBwuMhwuro+IcB5Lio0+MmXi+KNz5ubVO11OuWfv4RHHy+umvr/xizVvbvBPHbT0DD2oHMQEXWjKNFlEkGaEcfF4LlAMF5IgNYLQBOwkSQokOKCU0HXyqqCRLYTK7uw+fU1lVSt27jo+oElZEx8n945Ijz84f3H2f161+uq+40X1WsGR0gl1dbUX9fb25vkGjTxLt1ICul9YIJiWAJMDgJtBUkEEiEkR2b4MBaFC1aACCVgkFMOOU8QAWRCkmCRARAJOhwWXx+xxu10lHo/zWOa4jJJpk7OOzJw3sbf40PHYI2V1WfsLjy//ZPv+hX6/nmMaKtYyCIADlgFYUkIKqYglEwRLCQHAJCJmZtTVtV4YwEOCVUgCHSLIYGGFkpCLLSZmRWxZLAI6RwpS0wZ69WnNDe0PFB3tUB9/WNTgdFJ5XLw3f/TotJLll03fPH3q5M6CfUfjyytqJ9Q1tkwa8BkLfD5jqmUhQjdYWkqB7QRKQjEESAnFLBQZdmawhABAbEGTgMstlcsp6yLdkfsT4lOLc7ISS7+xemZte2eHc8/esvSmxs7cje9s+/Hrr22dHgiYo4KKPIYV1tWlnZ6FsIiY4FQkSBCUJkLVqVJ2Y+5MuWGpc4bwPAEPUTZWIAr1CxkQFN4AsiMnKZBQEGQxK4uViGBdSTIDllB+YzSRNbqz0395bW0X9u2tMKT2eY3H7Tg4evSIg7Mvnv3+td+89I+V5TXOXXsLxrS3d05qbffN8PusmYahjzR8AbdbUx6NdE2jgNstBbu8aHO5HBXR0TEFGRkpJWPGplYsWTajs+R4qfvQoaPjj1W2LNn2w/0PG6Y+yTRUrGlxSLySINKgGExSU3YlYRHsICWZQ205JrtOsKUtgFSoJxo6+c7zwvt85FkVSu2Kw41eu4AIaSkcTnYMsAVmi4gUKa3fTk8sQGwzacUOtkyQbpCDYY3vH9THd3ZX315SVskff/R5m9cpK2Jjo/emp6Udn7Q459nVa1a3Hj1Q5C04Ujg7EGhVxMGuyZNH/yglfVzF1avm1Z5ubtV27Cwc3dnrz/7802MPvf9efp5umKMty/KYlgbdZEASmBQLAWZSgNSI2Q5KgCXVWRzerhtsaJjCEwAqRFOtr9BYkheo0gSkDai93winMiYdZwqPsC6uAXDaoUCYNh0LhQXYehxoqFgjxUTQFcEIKCEgU3w+Tuno6V90sr4fzuIytenjL6q9noiCEampZSNSk3cvWj6j61Bha0fJ8dJlu/YcmB0c1KfphhVn5wDbg1k4IISHmSwlpEYMF5ESdjMbtqcSqbOEtTBdDBVrsEJJmkKc/+w50q9pQxcEcLZ5M4YEoFArmN0h7mvZR27Iy0N6m7I5Mg0V0uFjaadAAgtmApEDDAcUCbbsphozFII+SMGUMzBgZHW01YrmRse+nV9m3pd/sHRDr085FUn79AgHM5iFRmBWBOi2NAxIsC2rhUfmQApgC4AVWpPAkLQypI2EAD5baAxJFWd0AMChuS4M4FKI0ICBgIQCw7RHDUCh2ZAzQpC93FCTN7QJZ6q/oT3DUPM3pDMyGeGbGeokS3YCpLFFlsVKCoulzsphmewyTSgnCWEJIhGmk0rZ+vbQGQy/mbAAGGd5cGidLM+MVjCd9TsSgIAgBZAOiwSInZCmARlW7oCwvHEBANcc0DRAczh5SBgkAsEA+GyJ9iwLFZZny/TE4TmpcCUZOi2szhTlbG+tfdUz5Yt9wCzJUIJsngYBuzsTfvch7Qt0BlgK55hQWPxKGFGhjo44e9FfWa+CCElFGoTdARq6za7+3gsDuCALggiClKaUQUoxSJIiJia7xCPFiljQV6PaUDIKl+xnlEBbkDprFuUr2YjxD9MREUDhk3VG9Roqj742tk9fvwqfkZYBASV0+22ZzwomQzoPEzsZUMxKJ8V+AgIgAY9NDUvZ7T4vvM8d8KSEaERGuKFpjs7UlMg6rduXFtB9LsuQUBbBsBRIaqECQRLb7k920rTOCFcAAHVmED7M6+lsvMOCVUjt+0qOYqJQv3EokoY1krAegzDoDEUmzuwChVRDERLHFBQ0gB2htSl7PIiZFSuCsoQGP0lhQHNZ8Hi9Aa8nqjc1yfUp0SQGKuixJ5+7MID/+/3Xqdwp36Vbb7qt6IUnH5nxylsfpNafapnaP2gt7usdmGcYVmb/gD8moBuwLAEFDcoiVkIpEgAJQbAFFOKzgBEhLIZADSeusErIZ71sJzGh1P+92OCh2UIMpQMWHKJ6Ia3crv0ZYBYsWSkCK0MSWSSFSZpkuBwaPB5nX6TH2ZIQ79kXGevak5468tjNN66q31PD3Y7ECfT8w9eFn9A4Zzs/Evk18wd1cjsdDMD55cHikZs/3jFWWWJhV0/PsoEBX2YgYKQO+CCDhj1xxSAoC4qkxkT2ZAmRKcAqJBd8BTp7gcxQ5ACTqYTJIjEO+++4/Yp7X37p08M9fuWyW3JhoL8682nXCuEOFTNY2eNByiJACUEKTsFwOAC3UzPdbker26PVpKYk7vK63flLLpl34vJLpjUBCPiDBn3/4d+isek0BwJ+kZKcyEHd4OTEeHr+yfUXYNStqpvGZ8WyX4E+/ewQHSwopJXX/jtaW/vR0XjccLoTTv7lpfW1lz/44JfBX256FDdkxP5pw7uT6k8NTuzt7p/f3d2d5/cHRwQNK35gMAjdsEt2iy2QkEpqDraH7skeCggzhpADDc1oggErHCbOeD4NUTp7YBAMVkqxYBApU9gjsgZpGsPpJni8rt4Ir6c+Ltp9OCbGXZgxIq3w5htWVSTGRXYTESoqKpCbm8sAaOyk5XJMdi7lTZvMd951NV988RRO9oJL6ntJM7r4+SfPfTblvD38+MlmTBiThobWfhQerqKFF13EpzpbqKC4lPbszRelZTUoOVSkNJdLeb1uaqn/Ai77uRk+cLgy8/0Pt2X7A8ak/sHBVV3dPWMMdqb39QfdwaABQwnAppqskVBkT9WTgoOUMJQ0IRNj1Z47b1/57Zde2lbYa3t4mPiFuhBMrJQgAqQUcJIBr5N0t9tx2uN1VCQnxxUKjXevuOzi8m8subgJgG5aFuVOXYqOzh42fB0ie8pSMXliDhYunKsWzZvF8RGxSE0Hd/WA/vziq3zFzZdhSmb6+UL3zwH+j6yiI4CcBBdOnjZpbKrGTS061TSdpO079lN+QRUdPVbFnS0lyqJYHpOZRuVH3oXToTEAz6ubdmcfLSzP0oP63PaOnlmDA3pOMKDS/D6TlEUwEIDFAkSaJVjJmBjHl/d/+8YHnv/Pvx/pH7AiLGE3DAQsaMKCyyUQ6XX2Op2O8vjY+KPxsY6CzFEJhbfesLp+78cnerYd3EWXrczBlVdeyQAobezFYsLEaTQjbxJfeulinjo5m1PjXVxW00+TxkVxcU0bpmWl/FfA9F8H+NetrL4fxUW1iE4eB6/VRkvmT0NzUyt27DtEBwqOisNHy1B07KjydxxmAHzw4EHavs+PpZdewrmZnPD8H9+Z2HK6c/qg3ze5o69zgm5YE4N+FRfwD4iYWO/eO2+78d6XX3q1MOAnlyfKG9Rcsj7S6y2Oj4ss9zjk4eXLZhWvumxuAwCLmWnMxBWwLOb+7maRMXYq5Y7PpOWXLVTLFi7ins5yrPvJFpR8+igEgXv7LMTGXLgn2S4I4P/I9hysRmFJOe6+exU9dNOPseyedRgX58WughKRn19MR46UcmN1EQtPvPJ63NRV90WYAYjNOwpG7d55OKu3p2O+w0m9S5fNf//LbbvvlzLyRFb2yKM3rFlWN2D4+yrKT9HqK9dDDfYwYFDSuPFiQnYSFsybiksWzeWc8emcmZbMdW09NDophvNP1GPuhMz/Lgj+3wBnZmD3boLHw5ycTNTUBNXTA4qJAY0ZAxQXM9rbCbfeyv/o4VNmxubtJzBxwng4hEmjU9NQ13wSJ2u76JNPdtD+A8VUU9vK3Z0nFLQUdjkd1Nfy0dfZAN31WD0UO3jbW98TSYkRInvsSCxZMlctvnQx/3DtXbhv/c+waslkBuzHyqX8559A+5cYMyOwaTNxux/cEaDm3/1WHrx0mbwJoLwQCH9fvETWv/uOKH3xBRx9+qlz3lTLUuEnb7DjQDUBoKKaZnrh9U/FLff8WuZctFqmTbhUvLu5kGYtvFF6I2MdABz25DckADlmTIa2bt1aEW4SHDly8L/tFJ+Lnfdi+t//mLhvkIXTQd1Np2lkSqoCg+i2b56fTnmOdqysHlMnZoAAvPXBl5SSGInmrg60NrXB5XaylBL9fYP40UN3h++HAdC6devw2muvMQB69dVX+bbbbvtX4jxk5w14/dK5lPHlAZy6Yy05RmcoZ1bWnJ6yqm+J7t7xboNjdZcj2JeaWBW1YM7DfX39Na6+AZG7ahkjfhQDIH3PNgw2tZCMiOHoCVMAU7HV20Xa3Lk2vTtxAtzTQBQXzYCH/Ceq4fPriEhOhnv0aGw8ehjXr7meAXiWLFv0RHNT81jYdanShOxPS08/dv31171XdKyouq29nd579wMuLS2lSZMmMQB67bXXUF5eTikpKbxy5UrU19dzf38/ZWRkcExMDBUXF4OZcd111wEA5+fnk67rHB0dTVVVVXC73SguLuaenh668soredGiRRcOcGZG3zuf0fGX/kQyIlalzJt5n15R9J/kJb8W4TksLfQGlEzs8kaMSJ4zY21vV9c+h2HKrFlTLGdiIp149Gnx8bvvqe7eTv4NdDqwaI7IuWkd63Un1UBHO2XedDe0ZQuZiNC0/vey7p0PcfHxPVYoB9BLN1xNaTfeiCuvvkEBiE1LTzzW2zswMmPU6IOWZQnTtEZ1d3WNjImObr3mm99c0djYWNTZ2SnzLrrISk1Npeeee040NDQohCr7mJgYuXTpUt60aZMCQBkZGTh16hQDoEmTJlFZWZkCQMyM3NxcqqysPGuo7jw7D/+sh/tefVU4UxKVjIsZXf/im+WsqZLMJx69CoOdpwc3vC8iHvm5AuAGEGoFgfpf+TMGS49T19PPqIl27e1ipSx/TY3Z8PunyZOSgP7Kkzzp7b8B+w9S44FDNCrRw6Vr7+QUITQnEZUBRgCgk6/8le66404FIGpkWmoFSNQ2tbTMhx3DrauvuvqerVu3vDQqI+PtmpqaGwHIH/7whyoYDOLaa6/luXPnCofD4VZKGUeOHDFuuOEGSklJoTvvvFPNmDFD+v1+GhgYMDds2EBZWVkwDIMjIyPp5z//OZgZpaWljoMHDxper5dHjRpFixYtunBaCjOj6T+ekpaUFnncl6jt+3e401Of8b276XsDTqfTM2EK9xeXWnkflqjWjx+l1Id/CWbGn4kQCfCiJ5+9RtXUPwJpJQRZtxwRrk9H3nTDo9zf195zvEK88+2H1Py1d4uEqXnKGR09zSw5/lNnoHuOLhV3ud16MC3plYt++vPfheYPo9IS06uEQzQ0n26aBcDNzAGfz5eUnZ1d0dHRcUrX9elSSmmapgXAtWLFin+vqqq6DUCiZVm+pKSkXbfffvtvN27cWL5nzx5iZnnppZe+0tbWlr1ixYp1uq5X1tTUOGtra/WsrKxvVlVV/XbJkiX/6/XXX3+lv79ffPnll7xs2bLzAvy8GT7HJ7JIS4EjMqKu7VBpt2zruH3ks4+/R/kH9g109wp0Z6D946e1lJ89apU+/EvsXXkNTVy1RmXkTXmku7LiV7Ge6I1OZT5J0XKSv73zu9Ub3p6Q8+TTq2OzZgwsbejWDMM0yYnVrUcK39CUZaZEu//ugX7K8jjmdui+DB7qVgOWZGFZZnh81crNzUVOTs70vr6+uLy8vD/n5+fDsiwAoNmzZ79eUVFxXXp6+idut/sFIppWVVV1xxNPPLHkvvvuu2T9+vUnU1NTzVmzZm2ura19+7333nvx5MmTlwPQP//884y1a9e+pGla3+LFi7dmZ2ejoKAAEydOPF/4zj+ktG76hOoe/wk5Ukeo+KUrf0jHDjyhSAy64jL+QlOmbUi94/pjHxEh67H1FJ2SAIr0KOn15vR9tK1CxsW+tvPpP9w2blqeljB3rhmdMeJ660T1247csQ+Y9U0vJP3qpwgeLcnofP+zQgusZ9x1wxKru7dioKpOpH/nQQZAf/rzn/Ctu7+lAEQnpSYeN4IGfeOKVQ+3t7drfX19madOnbo3JSWl8M0337x+0qRJfRMnTuTp06ev+eijj96dNGnSr/Lz89cDcIwcOdKYN2/elR9//PFH2dnZfyouLr4HgAOAsXTp0kcLCgp+MWvWrO9v37796ZycnPc7OjquvvHGGy9qaWk51t3dLXfs2KHOV5r9pwAHgEDBftp5+cWgTvCsvzy9tLWs5geiR79MWIK93uiD0RdNWT/4u+d2NGYmOPRIj5E4fvxP+XTTb9KvXT7DV15VqMUlafF33WkCntim226p9kVHbVNbN9+sL78OUfExd3BD41+jJ477VrDp9J9la4dT725XDY3N3NHbo/T/eIauveF6BSA6IyW1sLWrc1xCcrKflZKGYTh8Ph/NmDHjzd///vffmTdvXg8ATktL26Lr+pLCwsIxGRkZrbNmzXIePnzYAmClpaUd7O3tzRocHEwHoJPdH0VWVtaX7e3teQsXLnxx7969P5k/f/53tm7d+lxiYqLW0dFhXnvttfTMM8/wqFGjLizgp7d9QQONTax5POTr6KAJD96nAEjf5nem9h8q/rdgbeNtuib8Ys60q5CU+PnA99cj9aqVz6L59ENGSuLxgPAEFAeFJNPU2OWyutqnDMbFf/LlCy9csfKaWxGVmf6o2dHxC++86XNVW0c+g2TvkULl/s6dULHx2FlcR7fddasCEJmamF4pHKK9uaVx+aZNm1xtbW3eN99885aCgoKfjxo1antFRcWlACgtLa2cmY3W1tbJAKizsxNpaWlS13VzxIgRfx0cHLxt27ZtmadOnWo4cuSINnfuXNOyrJH333//vtbW1ozp06dvOHr06B0AJLOtIm/bto0vu+yy83bW84rh3FWDQ5t2ImgacJsWRmaORvsvfqY1b9mmvEo/prkdt6c+eP9f2zZ//qVRXvmbrPse2te88VMf2CHclhPKFbmdNFe7yxKREBak5VEyK+GDQEJMfu5jP8bAzmNwUYpbSQKTtCynByyA7D/+Dh2lJ9Fc3QKlnfERU4OAZfQTURvODM6sHzt27MS2trY1d9xxx7QNGzYcGzFihDAMY6jTHh8fD13XbY8jchCRlZKSond2dqKjo4O2bNkCKWUyM3uEEDAMIzwLwUVFRWBmfuWVV/4panh+T0DEj0P4TXQHoc1g9QuAb5uTI7zpuVrrB5steeDu3RF33VGoD1hTO9/ZnNAbFeVzCXladzuRNG36S97aulLDL2TiE78aeprX2LqJ/H8/ovGIRNP0OupUmwEOBCZare2HB+vrRPOLf8c79UVqCsB4+cUhxAUMCDE0pyDj4uJoYGAgyMzdQgg4nU4JAIZhNFmWNbeioiI1Jyfn9MKFC10ADGYWaWlpeUKIutGjR7fC/tQDA0DsiBEj3iWizssvv/zFnTt3rl+wYEHp3r17f7N48WKtt7fXBIAXXngB999//3l7+bl7eMs2Ol66nyrKD6CqfF9kW2t57PGSz1BT/gVV3LhOVNx+B7o3vjX5+Npv+aru/8kXzEx9Gzei9eWX55bfdifXPfTdN5nZzczwvfkmcXU1cXU1uLqamv7wlGx+5UW0vPGnKTV33cO1d91b6GsoSOOjn6LhuquIWYmmulOejW9sFSG9JTIlNbklISFhR9h5oqOjsX79+jkxMTEDGRkZpcwcsWbNGqxatepur9fLs2bN+vXZjnbZZZfd4fV6eenSpd87+/XJkye/Hh0dzevWrVsFABMmTHg3MjKS161bd0VaWhoiIiJkb2/vP5X/zo+H97TT9seekaYUZlRq8r2Rtaf/M8Hy7dIjzCbJhu7xUXYgwAv7I2NaImbOujYYDOb379+rte/YYU64586n+2qqv+v1aE2e2KhPLF1VwtLGtDm1XBqf84OultNHIyqrND5Rbo66atW/BctP/oeQ8LmiI7cIzdV2msQlvdGRxYsf+cla2AVVbFJK6rHB/r70lJTkz/z+gNQ0Lck0zZkul6vxmmuuub6wsPBAiF878/Ly3qmqqroyKSlpe0xMzE6/3z+jtbX1qpEjR24pLS29noj8AHj27NmPlJWV/So7O/uZY8eOfY+ZsW3bthFr167NN00z9vbbb1/Q0tJyrLm5Waxdu5bvueeeC8nD3RiVNEKZTg9ccVEHcKrnfQP9YwIBIxsMoaRscqcnvjx2xtQ/6BUVJ/Ren8h+8lELCS8CwE/qnv2Pff0NJ9cFfL2zoeQyItegIGeplFqf5nbDO3euKnjlT/TrA3ufffUPL1XrNXXrlD44xm+qqeTxtkR6XB99m0j90S58glMnTf24qakhG8QRLpfb1DTtZEZGxl8ffvjh906ePNnW29sr5s+fzwCCL7/88i0/+tGP7mtoaLiuq6vrNofD0TFz5szvffjhh38kosD27ds5Kioq6Qc/+MGU3NzcDY8//vhjLS0tuO6667TOzs6mBQsW3FFdXf1IW1vbsrfeeusYAFxzzTUX1sO/4u0cnmmyP/Eg9LUFgEq/9ytIpxOdVbU8fd1KBAcDVPn39xA9djQP1jfQ7E1vDV0GsLvrgb1fkN7ZwyQlnfrbxxQ3Y5Iy+32U8egj5+NBQ2rhypUr6ZNPPlETJ06EpmlUV1cHl8vFt9xyCz3zzDOIiYnBwoULuaGhQSxYsIDHjBnDV1xxhaisrOTnn3+eDx8+LNauXctSSh47diwFg0EMDg6yaQ5NXTEAevzxxy9gad/Tg7fmfJNcnkhOuTiPGt97l5ytZdwI8IMADIAaL5onou++nXfef6+KAmjBli3wZmQwlKKKDa+J2uef4yg9yAF7JpVOpeaICT/5njp5+CiPHj2C5v765wyAGr73iKh74S84ETytLh2ZyVv7+8XI1Vfhqtf+wv0AP/XUenpi/ZPk99kfn2E3nJlSU1Pxne98h8vLy1VzczM5nU5ERETwpEmT6KWXXhKtra1DH6uakJAgfvzjH6v6+nrev38/HT16lAFQcnIyXXfdddzb28tvvPEGwebyoqWlBZqmsWma/7SAdd4eHjhwGHB5AAEyKusQ4AGww/4wRV9LM2IyMhG76hu2Bxw6BMxOxukPj8AdlwwZ7aS2gnwwGM6IWJiDfnji4iEm5EC6PWyeakCKoUCxMYCm0WBJOYIOAV0K6P4AYsaORbCzg5Ov+SYA0M9/sh6d3e0ImDo8bjeysrKwZMkSDAwM8OnTp5GXl4dx48bh1ltvxc6dO/HUU09RZWUl/H4/GhsbsWbNGlRXV7PP56PIyEhOT0+nvXv3gojw7LPPMgA8+eSTNDAwwMnJyVRSUoLk5GQ89thjzMx44YUX8MADD5wvhMM2bMM2bMM2bMM2bMM2bMM2bMM2bMM2bMM2bMM2bAD+NzjkVzAbsi2QAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIzLTEyLTEzVDEwOjQ2OjE2KzAwOjAwBeteowAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMy0xMi0xM1QxMDo0NjoxNiswMDowMHS25h8AAAAASUVORK5CYII="
                                        alt="Logo">
                                    <p class="company">SecBox GmbH & Co. KG<br>
                                    Lohrtalweg 10, 06261 Mosbach / Germany<br>
                                    <a
                                        href="mailto:information.secbox@gmail.com">information.secbox@gmail.com</a><br>
                                    <a href="http://192.168.56.101">http://192.168.56.101</a>
                                    </p>
                                    <br>
                                    <p class="company">A company of Duale Hochschule Baden-Württemberg</p>
                                    <div class="line"></div>
                                    <br>
                                    <div class="small-text">
                                    <z class="company">SecBox GmbH & Co. KG<br>
                                        Lohrtalweg 10, 06261 Mosbach / Germany<br>
                                        <a
                                            href="mailto:information.secbox@gmail.com">information.secbox@gmail.com</a><br>
                                        <a href="http://192.168.56.101">http://192.168.56.101</a>
                                    </z>
                                    <br><br>
                                    <z class="company">Personally liable and managing partner:<br>
                                    </z>
                                    <z>SecBox Holding GmbH<br>
                                        Lohrtalweg 10, 06261 Mosbach / Deutschland
                                    </z>
                                    <br>
                                    <z class="company">Board of directors:</z>
                                    <br>
                                    <z>Sepp Maier<br>
                                        <br>
                                            This e-mail contains confidential and information. If you are not the
                                            intended recipient (or have received this e-mail in error) please notify
                                            the sender immediately and destroy this e-mail.
                                        </z>
                                        <br><br>
                                        <z class="environment">Please consider the environment before printing this
                                            e-mail.</z>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
"""

class Mail:

    def __init__(self):
        """
            Diese Klasse ist für das Versenden von E-Mails verantwortlich.
            Parameter:  NONE
        """



    # Methode itteriert über die E-Mail-Adressen
    # :param eventnumber: Ereignisnummer
    #
    def send_Mail_3_6(self, eventnumber):
        übergabe = eventnumber
        Email_adressen = self.databaseobject.get_email_array()

        for Email in Email_adressen:
            self.send_Mail(übergabe, Email)


        
    # Methode zum Versenden von E-Mails
    # :param eventnumber: Ereignisnummer
    # :param receiver: Empfänger
    # :return: True, wenn die E-Mail erfolgreich gesendet wurde
    #
    def send_Mail(self, eventnumber, receiver):
        
        port = os.getenv ("port")
        smtp = os.getenv ("smtp") 
        password = os.getenv ("password")  
        sender_email = os.getenv("sender_email") 
        friendly_name = os.getenv("friendly_name")
        receiver_email = receiver 
        receiver_email = receiver  # Empfänger email
        
        text = 'text'
        html = 'html'

        message = MIMEMultipart("alternative")
        message['From'] = f"{friendly_name} <{sender_email}>"
        message['To'] = receiver_email
        
        # Fall 1 : Welcome Message (erster benutzer)
        if eventnumber == 1:
            message["Subject"] = "Willkommen Bei SecBox!"  
            text="""\
            Liebe Kundin, lieber Kunde, 

            🥳🎉 Wir freuen uns, Sie bei SecBox willkommen zu heißen 🎊🍾

            Vielen Dank, dass Sie unseren Service gewählt haben. Wir setzen uns dafür ein, Ihnen ein außergewöhnliches Erlebnis zu bieten und sind zuversichtlich, dass Sie von den vielen Vorteilen profitieren werden, die wir bieten.

            Wir freuen uns, Sie an Bord zu haben und freuen uns darauf, Sie auf Ihrer Reise mit Ihrer Paketbox zu begleiten. Entdecken Sie die Möglichkeiten und erleben Sie den Unterschied mit uns!"""

            html = html_head + html_message_1 + html_signature

        # Fall 2 : Registration Message (weitere benutzer)
        if eventnumber == 2:
            message["Subject"] = "Registration erfolgreich!"  
            text="""\
            Liebe Kundin, lieber Kunde,

            wir freuen uns, bestätigen zu können, dass Ihre E-Mail-Adresse erfolgreich bei SecBox registriert wurde. Vielen Dank, dass Sie diesen Schritt unternommen haben, um mit uns in Verbindung zu bleiben.

            Was kommt als Nächstes?

            Bleiben Sie informiert: Sie erhalten nun Updates und wichtige Benachrichtigungen direkt in Ihren Posteingang.

            Wir setzen uns dafür ein, Sie über die neuesten Entwicklungen bei SecBox informiert und verbunden zu halten. Wenn Sie Fragen haben oder Unterstützung benötigen, steht Ihnen unser Kundenservice-Team jederzeit zur Verfügung.

            Vielen Dank, dass Sie unserer Gemeinschaft beigetreten sind, und wir freuen uns darauf, mit Ihnen in Kontakt zu treten."""

            html = html_head + html_message_2 + html_signature
            
        # Fall 3 : The scanner detected an unexpected package
        if eventnumber == 3:
            message["Subject"] = "Unerwartetes Paket!"  
            text="""\
            Liebe Kundin, lieber Kunde, 
            
            wir haben eine wichtige Nachricht für Sie.
            
            Wir möchten Sie darüber informieren, dass ein unerwartetes Paket an Ihrer Paketbox gescannt wurde!
            
            Dies könnte auf eine Fehllieferung oder eine nicht registrierte Sendung hinweisen. Wir bitten Sie, diesen Sachverhalt zu überprüfen und geeignete Maßnahmen zu ergreifen, um die Sicherheit und Genauigkeit der Lieferungen zu gewährleisten.
            
            Bitte überprüfen Sie die Situation umgehend und stellen Sie sicher, dass alle Sendungen korrekt registriert und zugewiesen sind. Ihre schnelle Reaktion ist entscheidend, um Unregelmäßigkeiten zu vermeiden."""
            
            html = html_head + html_message_3 + html_signature
        
# Fall 4 : The package box has been open for more than 3 minutes
        if eventnumber == 4:
            message["Subject"] = "Paketbox überprüfen!"  
            text="""\
            Liebe Kundin, lieber Kunde, 

            wir haben festgestellt, dass Ihre Paketbox seit mehr als 3 Minuten geöffnet ist!

            Dies könnte auf ein Sicherheitsrisiko oder einen Defekt hinweisen. Bitte überprüfen Sie umgehend die Situation und ergreifen Sie die notwendigen Maßnahmen, um Sicherheit und reibungslosen Betrieb zu gewährleisten.

            Vielen Dank für Ihre prompte Antwort."""
            
            html = html_head + html_message_4 + html_signature
            
# Fall 5 : Error Button was pressed
        if eventnumber == 5:
            message["Subject"] = "Fehler-Taste betätigt!"  
            text="""\
            Liebe Kundin, lieber Kunde, 

            wir haben eine wichtige Nachricht für Sie.

            Wir möchten Sie darüber informieren, dass der Fehlerknopf an Ihrer Paketbox gedrückt wurde!

            Bitte informieren Sie uns umgehend über den aktuellen Status, damit wir angemessene Unterstützung leisten oder notwendige Maßnahmen ergreifen können. Wir schätzen Ihr schnelles Handeln in dieser Situation und stehen bereit, um jegliche benötigte Unterstützung anzubieten.

            Sollten Sie Fragen haben oder weitere Anweisungen benötigen, stehen wir Ihnen jederzeit zur Verfügung."""  

            html = html_head + html_message_5 + html_signature

# Fall 6 : A package was successfully delivered
            # Falscher Text Felix To do 
        if eventnumber == 6:
            message["Subject"] = "Paket erfolgreich eingetroffen!"  
            text="""\
            Liebe Kundin, lieber Kunde, 

            wir freuen uns, Ihnen mitteilen zu dürfen, dass Ihr Paket erfolgreich zugestellt wurde.

            Wir hoffen, dass alles zu Ihrer Zufriedenheit verlaufen ist und bedanken uns für Ihr Vertrauen in unseren Service.

            Für Rückmeldungen oder Fragen stehen wir Ihnen jederzeit zur Verfügung."""

            html = html_head + html_message_6 + html_signature
            
            
        ##
        # Turn these into plain/html MIMEText objects
        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        message.attach(part1)
        message.attach(part2)

        # Create secure connection with server and send email
        ssl_context = ssl.create_default_context()
        ssl_context.check_hostname = False
        # needed to connect through the VM to google Server
        ssl_context.verify_mode = ssl.CERT_NONE
        with smtplib.SMTP(smtp, port) as server:
            server.starttls(context=ssl_context)
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string())
            
        return True