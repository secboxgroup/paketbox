import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
import threading
import subprocess
from DATABASE import Database



# GPIO-Konfiguration
GPIO.setmode(GPIO.BCM)
FEHLER_TASTER = 12
PACKETBOXDOOR = 6
HERUNTERFAHREN_TASTER = 20
DOOR_LOCK = 4
STATUS_LED = 13
ERROR_LED = 18
TRAIL_LED = 17
GPIO.cleanup()



# Konfigurieren der Pins als Ausgänge
GPIO.setup(DOOR_LOCK, GPIO.OUT)
GPIO.setup(STATUS_LED, GPIO.OUT)
GPIO.setup(ERROR_LED, GPIO.OUT)



# Konfigurieren der Pins als Eingänge
GPIO.setup(FEHLER_TASTER, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(PACKETBOXDOOR, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(HERUNTERFAHREN_TASTER, GPIO.IN, pull_up_down=GPIO.PUD_UP)



class GpioHandler:



    def __init__(self, databaseobject: Database):
        """
            Klasse zum Steuern der GPIOs
            Parameter:  databaseobject: Ein Datenbankobjekt, das die Daten für die GPIOs bereitstellt.
        """
        self.databaseobject = databaseobject
        
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message


        #Erstellen der Interrupt Aufrufe 
        GPIO.add_event_detect(PACKETBOXDOOR, GPIO.RISING, callback=self.interrupt_callback_falling, bouncetime=900)
        GPIO.add_event_detect(FEHLER_TASTER, GPIO.FALLING, callback=self.interrupt_callback_falling, bouncetime=900)
        GPIO.add_event_detect(HERUNTERFAHREN_TASTER, GPIO.FALLING, callback=self.interrupt_callback_falling, bouncetime=900)        

        #GPIO.add_event_detect(PACKETBOXDOOR, GPIO.RISING, callback=self.interrupt_callback_rising, bouncetime=300)


    # Callback-Funktion für das Abonnieren von MQTT-Themen
    # :param client: Der MQTT-Client
    # :param userdata: Benutzerdaten
    # :param flags: Flags
    # :param rc: Rückgabecode
    #
    def on_connect(self, client, userdata, flags, rc):
        self.client.subscribe("SYS/PI/#")
        self.client.subscribe("SYS/API/#")

    

    # Callback-Funktion für den Empfang von MQTT-Nachrichten
    # :param client: Der MQTT-Client
    # :param userdata: Benutzerdaten
    # :param msg: Die Nachricht
    #
    def on_message(self, client, userdata, msg):
        # Die Box wird auf Anfrage der Api entriegelt
        if msg.topic == "SYS/API/BOXDOORLOCK" and msg.payload.decode("utf-8") == "True":
            self.set_door_lock()
            self.client.publish("SYS/GPIO/BOXDOORLOCK", "True", 1)

        # Die Box wird auf Anfrage des Pi entriegelt
        elif msg.topic == "SYS/PI/BOXDOORLOCK" and msg.payload.decode("utf-8") == "True":
            self.set_door_lock()
            self.client.publish("SYS/GPIO/BOXDOORLOCK", "True", 1)
            
        # Die Box wird auf Anfrage des Pi verriegelt
        elif msg.topic == "SYS/PI/BOXDOORLOCK" and msg.payload.decode("utf-8") == "False":
            self.reset_door_lock()
            self.client.publish("SYS/GPIO/BOXDOORLOCK", "False", 1)
            
        # Die Status-LED wird auf anfrage des Pi abeschaltet
        elif msg.topic == "SYS/PI/STATUSLED" and msg.payload.decode("utf-8") == "False":
            self.reset_status_led()
        
        # Die Status-LED wird auf anfrage des Pi eingeschaltet
        elif msg.topic == "SYS/PI/STATUSLED" and msg.payload.decode("utf-8") == "True":
            self.set_status_led()

        # Die ERROR-LED wird auf anfrage des Pi eingeschaltet
        elif msg.topic == "SYS/PI/ERRORLED" and msg.payload.decode("utf-8") == "True":
            self.set_error_led()

        # Die ERROR-LED wird auf anfrage des Pi abgeschaltet
        elif msg.topic == "SYS/PI/ERRORLED" and msg.payload.decode("utf-8") == "False":
            self.reset_error_led()



    # Startet den MQTT-Thread
    #
    def start_mqtt_thread(self):
        mqtt_thread = threading.Thread(target=self.loop_forever)
        mqtt_thread.start()



    # Funktion zum Verbinden mit dem MQTT-Broker
    #
    def connect_to_mqtt_broker(self):
        self.client.connect("localhost", 1883 , 60)



    # Übergabe der Loop-Funktion von Paho
    #
    def loop_forever(self):
        self.connect_to_mqtt_broker()
        self.client.loop_forever()


    # Setzt die Status-LED
    #
    def set_status_led(self):
        GPIO.output(STATUS_LED, GPIO.HIGH)



    # Setzt die Status-LED zurück
    #
    def reset_status_led(self):
        GPIO.output(STATUS_LED, GPIO.LOW)



    # Setzt die ERROR-LED
    #
    def set_error_led(self):
        GPIO.output(ERROR_LED, GPIO.HIGH)



    # Setzt die ERROR-LED zurück
    #
    def reset_error_led(self):
        GPIO.output(ERROR_LED, GPIO.LOW)



    # Setzt das Türschloss
    #
    def set_door_lock(self):
        GPIO.output(DOOR_LOCK, GPIO.HIGH)



    # Setzt das Türschloss zurück
    def reset_door_lock(self):
        GPIO.output(DOOR_LOCK, GPIO.LOW)



    # Callback-Funktion für das Abfangen von Interrupts
    # :param channel: Der GPIO-Pin
    #
    def interrupt_callback_falling(self, channel):
        if channel == 6:
            self.client.publish("SYS/GPIO/BOXDOORSENSOR", "True", 1)
        if channel == 12:
            self.client.publish("SYS/GPIO/ERRORBUTTON", "True", 1)
        if channel == 20:
            self.databaseobject.write_packagebox_status(0)
            print("Paketboxstatus zurückgesetzt")
            self.cleanup()
            self.shutdown() 



    # Cleanup-Funktion für die GPIOs
    #
    def cleanup(self):
        GPIO.cleanup()



    # Funktion zum Herunterfahren des Raspberry Pi
    #
    def shutdown(self):
        
        subprocess.run(['sudo', 'shutdown', '-h', 'now'], check=True)
        






