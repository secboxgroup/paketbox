import threading
from SENDMAIL import Mail
from DATABASE import Database

# Diese Klasse übernimmt die Logik der Paketbox(Timer + Mails versenden)


class Packagebox:

    def __init__(self, mailobject: Mail, databaseobject: Database):
        """
            Diese Packagebox Klasse übernimmt die Logik der Paketbox(Timer + Mails versenden)
            Parameter:  mailobject: Benötigt ein Mailobjekt(SENDMAIL)
                        databaseobject: Benötigt ein Datenbankobjekt(DATABASE) 
        """
        # Übergabe der zu integrierenden Objekte
        self.mailobject = mailobject 
        self.databaseobject = databaseobject

    
    # Startet den Timer zum überprüfen des Öffnungszeitenlimits(3 Minuten = 180 Sekunden).
    # Wenn der Timer überschritten wird, wir die Funktion boxOpenTooLong aufgerufen.
    # :param: None
    # :return: VOID   
    def unlockBox(self):
 
        self.timer = threading.Timer(60, self.box_lock_open_too_long)
        self.timer.start()

    # Wird aufgerufen wenn die Box zu lange offen ist.
    # Sendet eine E-Mail und schaltet den Timer aus.
    # :param: None
    # :return: VOID   
    def box_lock_open_too_long(self):

        emails = self.databaseobject.get_email_array()

        for mailadresse in emails:
            self.mailobject.send_Mail(4, (mailadresse))

        if self.timer.is_alive():
            self.timer.cancel()


    # Schließt den Timer falls noch ein Thread besteht.
    # :param: None
    # :return: VOID   
    def lockBox(self):

        try:
            self.timer.cancel()

        except:
            pass