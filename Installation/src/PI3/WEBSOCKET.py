from flask import Flask
from flask_socketio import SocketIO, emit
import json



class Websocket:



    def __init__(self):
        """
            Diese Klasse ist für die Kommunikation mit der Website über das Websocket verantwortlich.
            Parameter:  NONE
        """
        self.app = Flask(__name__)
        self.socketio = SocketIO(self.app, cors_allowed_origins="*")
        # Bei Vernindung mit Website wird die handle_connect Methode aufgerufen
        self.socketio.on_event('connect', self.handle_connect, namespace='/ws')
        # Bei Trennung von Website wird die handle_disconnect Methode aufgerufen
        self.socketio.on_event('disconnect', self.handle_disconnect, namespace='/ws')
        #Startet Websocket beim aufruf
        

        
    # Methode zum Senden an die Website 
    # :param event: Art des Events   
    # :param data_key: Schlüssel des Datenobjekts
    # :param data_value: Wert des Datenobjekts
    #
    def emit_data_update(self, event, data_key, data_value):
        # Erstellt ein Dictionary mit den Daten
        data = {data_key: data_value}
        self.socketio.emit(event, json.dumps(data), namespace='/ws')



    # Methode zum Senden der Temperatur
    # :param temperature: Temperaturwert
    #
    def send_temperature(self, temperature):
        self.emit_data_update('message', 'temperatur', temperature)
        


    # Methode zum Senden der Klingelmeldung
    # :param bell_status: Status der Klingel
    #
    def send_bell(self, bell_status):
        self.emit_data_update('message', 'bell', bell_status)
        


    # Methode zum Senden des Lichtstatus
    # :param light_status: Status des Lichts
    #
    def send_light(self, light_status):
        self.emit_data_update('message', 'light', light_status)



    # Methode zum Senden des Boxstatus
    # :param box_status: Status des Paketkastens
    #  
    def send_boxstatus(self, box_status):
        self.emit_data_update('message', 'boxstatus', str(box_status))



    # Methode zum Senden des Paketstatus
    # :param package_status: Status des Pakets
    # :param package_number: Paketnummer
    #    
    def send_packagestatus(self, package_status, package_number):
        package_data = {
            'status': package_status,
            'package_number': package_number
            }
        # self.emit_data_update('message', 'packagestatus', package_data)
        self.socketio.emit('message', json.dumps(package_data), namespace='/ws')



    # Methode zum Senden des Fehlerbuttonstatus
    # :param status: Status des Fehlerbuttons
    #
    def send_error_button(self, status):
        self.emit_data_update('message', 'errorbutton', status)



    # Methode zum Senden der Warnung wenn die Paketbox zu lange offen ist
    # :param status: Status der Warnung
    #
    def send_open_too_long(self, status):
        self.emit_data_update('message', 'toolong', status)



    # Methode zum Senden des Barcode-Scans
    # :param status: Status des Barcode-Scans
    #
    def send_barcode_scanned(self, status):
        self.emit_data_update('message', 'scanned', status) 



    # Methode zum Senden des Öffnungsstatus der Paketbox
    # :param status: Status der Öffnung
    #
    def send_packagebox_opened(self, status):
        self.emit_data_update('message', 'opened', status)



    # wird aufgerufen, wenn Website sich verbindet
    #
    def handle_connect(self):
        print('Client connected')
     


    # wird aufgerufen, wenn website sich trennt 
    #  
    def handle_disconnect(self):
        print('Client disconnected')
  


    # Startet den Websocket
    # :host: localhost
    # :port: 8080
    # :allow_unsafe_werkzeug: True
    #
    def run(self):
        self.socketio.run(self.app, host='localhost', port=8080, allow_unsafe_werkzeug=True)
        
