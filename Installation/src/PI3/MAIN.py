import threading
import signal
import sys
from dotenv import load_dotenv
from MQTTMAINCLASS import MqttMainClient
from PACKAGEBOX import Packagebox
from DATABASE import Database
from WEBSOCKET import Websocket
from RESTAPI import REST_API
from FLASKAPP import Flaskhandler
from SCANNER import BarcodeReader
from SENDMAIL import Mail
from GPIOHANDLER import GpioHandler
from THINKSPEAK import ThingSpeakClient



load_dotenv()



# Erstellen des Signal_handlers.
# Durch eingabe von Strg+C soll der Boxstatus, sowie alle GPIOs zurückgesetzt werden.
def signal_handler(sig, frame):
    db1.write_packagebox_status(0)
    gpi1.cleanup()
    sys.exit(0)



# Ausführen des Eventhandlers
signal.signal(signal.SIGINT, signal_handler)



# Erzeugen der benötigten Objekte(benötigt für MQTT deswegen als erstes)
wb1 = Websocket()
m1 = Mail()
db1 = Database(wb1, m1)
pb1 = Packagebox(m1,db1)
gpi1 = GpioHandler(db1)
ts1 = ThingSpeakClient(db1)



#Erzeugen der MQTT Clients
main_mqtt = MqttMainClient("MAIN", db1, wb1, pb1, False)
api_mqtt = MqttMainClient("API", db1, wb1, pb1, True)
scanner_mqtt = MqttMainClient ("SCANNER", db1, wb1, pb1, True)



# Scanner Objekt erzeugen (benötigt MQTT deswegen später aufruf)
sc1 = BarcodeReader(scanner_mqtt)


# Funktion zum starten vom Websocket
# :param: None
# :return: VOID
#
def start_websocket_server():
    wb1.run()


# Funktion zum starten von Flask
# :param: None
# :return: VOID
#
def start_flask_app():
    api = REST_API(db1, api_mqtt)
    flask1 = Flaskhandler(api)
    flask1.run() 



# Starte Threads
websocket_thread = threading.Thread(target=start_websocket_server)
websocket_thread.start()

scanner_thread = threading.Thread(target= sc1.barcode_reader)
scanner_thread.start()

gpi1.start_mqtt_thread()

flask_thread = threading.Thread(target=start_flask_app)
flask_thread.start()

thingspeak_thread = threading.Thread(target=ts1.post_status_periodically)
thingspeak_thread.start()



# Verbindung zum MQTT-Broker und Start der Main-MQTT-Schleife
main_mqtt.connect_to_mqtt_broker()
main_mqtt.loop_forever()