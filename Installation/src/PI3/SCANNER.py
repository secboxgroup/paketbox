from MQTTMAINCLASS import MqttMainClient



# Mapping für die Charaktere
# Quelle: https://www.raspberrypi.org/forums/viewtopic.php?f=45&t=55100 



# Kleinbuchstaben und Zahlen
hid = {4: 'a', 5: 'b', 6: 'c', 7: 'd', 8: 'e', 9: 'f', 10: 'g', 11: 'h', 12: 'i', 13: 'j', 14: 'k',
                     15: 'l', 16: 'm', 17: 'n', 18: 'o', 19: 'p', 20: 'q', 21: 'r', 22: 's', 23: 't', 24: 'u', 25: 'v',
                     26: 'w', 27: 'x', 28: 'y', 29: 'z', 30: '1', 31: '2', 32: '3', 33: '4', 34: '5', 35: '6', 36: '7',
                     37: '8', 38: '9', 39: '0', 44: ' ', 45: '-', 46: '=', 47: '[', 48: ']', 49: '\\', 51: ';',
                     52: '\'', 53: '~', 54: ',', 55: '.', 56: '/'}



# Großbuchstaben und Sonderzeichen
hid2 = {4: 'A', 5: 'B', 6: 'C', 7: 'D', 8: 'E', 9: 'F', 10: 'G', 11: 'H', 12: 'I', 13: 'J', 14: 'K',
                     15: 'L', 16: 'M', 17: 'N', 18: 'O', 19: 'P', 20: 'Q', 21: 'R', 22: 'S', 23: 'T', 24: 'U', 25: 'V',
                     26: 'W', 27: 'X', 28: 'Y', 29: 'Z', 30: '!', 31: '@', 32: '#', 33: '$', 34: '%', 35: '^', 36: '&',
                     37: '*', 38: '(', 39: ')', 44: ' ', 45: '_', 46: '+', 47: '{', 48: '}', 49: '|', 51: ':', 52: '"',
                     53: '~', 54: '<', 55: '>', 56: '?'}



carriage_return = 40  # Zeichen für Carriage Return, das das Ende eines Barcodes kennzeichnet
shift = 2  # Zeichen, das angibt, dass das nächste Zeichen das Großbuchstaben-Mapping verwenden soll
error = '?'  # Platzhalter für nicht erkannte Zeichencodes



class BarcodeReader:



    def __init__(self, mqttobject: MqttMainClient):
        """
            Diese Klasse ist für das Lesen von Barcodes über den HID-Barcode-Scanner verantwortlich.
            Parameter:  mqttobject: Ein MqttMainClient-Objekt, das die Kommunikation mit dem MQTT-Broker ermöglicht.
        """
        fp= "/dev/hidraw0"
        self.fp = fp
        self.f = open(self.fp, "rb")
        self.mqttobject = mqttobject



    # Methode zum Lesen von Barcode-Daten
    # :return: Decodierter Barcode-String
    #
    def read_barcode(self):
        """
        Liest Barcode-Daten vom Scanner und gibt den decodierten String zurück.
        """
        scanned_barcode = ''
        charmap = hid
        while True:
            for char_code in [element for element in self.f.read(8) if element > 0]:
                if char_code == carriage_return:
                    # Carriage Return erkannt, Ende des Barcodes
                    return scanned_barcode
                if char_code == shift:
                    # Nächstes Zeichen sollte mit Großbuchstaben-Mapping verwendet werden
                    charmap = hid2
                else:
                    # Fügt das Zeichen oder einen Platzhalter zum Ausgabestring hinzu
                    scanned_barcode += charmap.get(char_code, error)
                    # Zurücksetzen auf Kleinbuchstaben für nachfolgende Zeichen
                    charmap = hid



    # Methode zum kontinuierlichen Lesen von Barcodes und Senden der Daten an den MQTT-Broker
    #
    def barcode_reader(self):

        while True:
            scanned_barcode = self.read_barcode()
            print("string" + scanned_barcode)
            # Sendet eine vordefinierte Nachricht und die Barcode-Daten an den MQTT-Broker
            self.mqttobject.send_message("SYS/SCANNER", scanned_barcode, 1)
            # self.f.close()