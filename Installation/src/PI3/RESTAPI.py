from flask import jsonify,request
from DATABASE import Database
from MQTTMAINCLASS import MqttMainClient
import time



class REST_API:



    def __init__(self, databaseobject: Database, mqttobject: MqttMainClient):
        """
            Initialisiert die REST_API Klasse mit den benötigten Objekten. 
            Parameter:  databaseobject: Ein Datenbankobjekt, das die Daten für die API bereitstellt.
                        mqttobject: Ein MqttMainClient-Objekt, das die MQTT-Kommunikation für die API bereitstellt.
        """
        self.databaseobject = databaseobject
        self.mqttobject = mqttobject
        


    # Nimmt die Benutzerdaten entgegen und speichert sie in der Datenbank.
    # :param: data: Ein JSON-Objekt, das die Benutzerdaten enthält.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    #
    def get_user_data(self,data):
        result = self.databaseobject.create_user(data)
        if result:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"error": "failure"}), 500



    # Gibt die aktuellen Zeiten zurück.
    # :return: Ein JSON-Objekt, das die aktuellen Zeiten enthält.  
    #
    def show_time(self):
        result = self.databaseobject.get_operating_time()
        return result,200



    # Gibt die E-Mail-Adressen zurück, die in der Datenbank gespeichert sind.
    # :return: Ein JSON-Objekt, das die E-Mail-Adressen enthält.
    #
    def email_list(self):
        result = self.databaseobject.get_email_list()
        return result,200



    # Gibt die Systemlogs zurück.
    # :return: Ein JSON-Objekt, das die Systemlogs enthält.
    #
    def system_log(self):
        result = self.databaseobject.get_system_logs()
        return result,200



    # Nimmt die Daten eines neuen Pakets entgegen und speichert sie in der Datenbank.
    # :param: data: Ein JSON-Objekt, das die Paketdaten enthält.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    #
    def add_package(self,data):
        result = self.databaseobject.add_package(data)
        if result == True:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"error": "failture"}), 500



    # Nimmt die Daten eines zu löschenden Pakets entgegen und löscht sie aus der Datenbank.
    # :param: data: Ein JSON-Objekt, das die Paketdaten enthält.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    #
    def delete_package(self,data):
        result = self.databaseobject.delete_package(data)
        if result == True:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"error": "failture"}), 500
        


    # Gibt alle Pakete zurück.
    # :return: Ein JSON-Objekt, das die Pakete enthält.
    #
    def all_packages(self):
        result = self.databaseobject.get_open_packages()######all packets
        return result



    # Gibt alle offenen Pakete zurück.
    # :return: Ein JSON-Objekt, das die offenen Pakete enthält.
    #
    def open_packages(self):
        result = self.databaseobject.get_open_packages()
        return result, 200



    # Gibt die letzten fünf Pakete zurück.
    # :return: Ein JSON-Objekt, das die letzten fünf Pakete enthält.
    #
    def last_five_packages(self):
        result = self.databaseobject.get_last_five_packages()
        return result,200
    


    # Öffnet die Gartentür
    # :param: data: Ein JSON-Objekt, das die Anweisung zur Öffnung der Gartentür enthält.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    #
    def open_garden_door(self,data):
        self.mqttobject.send_message("SYS/API/DOOR", "True", 1)
        result = self.databaseobject.write_door_status(1)
        if result == True:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"error": "failture"}), 500



    # Öffnet die Paketbox
    # :param: data: Ein JSON-Objekt, das die Anweisung zur Öffnung der Paketbox enthält.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    #
    def open_packet_box(self,data):
        self.mqttobject.send_message("SYS/API/BOXDOORLOCK", "True", 1)
        time.sleep(0.5)
        result = self.databaseobject.get_packageboxdoor_lock()
        if result == 1:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"error": "failture"}), 500



    # Prüft ob ein Benutzer bereits registriert ist.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    #
    def already_registered(self):
        result = self.databaseobject.check_user()
        if result == True:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"error": "failture"}), 404



    # Nimmt die E-Mail-Adressen entgegen und löscht sie aus der Datenbank.
    # :param: data: Ein JSON-Objekt, das die E-Mail-Adressen enthält.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    def deleteemails(self,data):
        email_addresses = data["emails"]
        result = self.databaseobject.delete_emails(email_addresses)
        if result == True:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"status": "failure"}), 500



    # Nimmt die Benutzerdaten entgegen und aktualisiert sie in der Datenbank.
    # :param: data: Ein JSON-Objekt, das die Benutzerdaten enthält.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    #
    def get_update_user_data(self,data):
        result = self.databaseobject.update_user(data)
        if result == True:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"status": "failure"}), 500



    # Nimmt die Zeiten entgegen und aktualisiert sie in der Datenbank.
    # :param: data: Ein JSON-Objekt, das die Zeiten enthält.
    # :return: Ein JSON-Objekt, das den Status der Anfrage enthält.
    #
    def switchtimes(self,data):
        result = self.databaseobject.write_time_status(data['toggletime'])
        if result == True:
            return jsonify({"status": "success"}), 200
        else:
            return jsonify({"status": "failure"}), 500