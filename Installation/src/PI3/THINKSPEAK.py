from dotenv import load_dotenv
from DATABASE import Database
from time import sleep
import urllib.request
import os
#import threading


load_dotenv()

class ThingSpeakClient:



    def __init__(self, databaseobject: Database):
        """
            Diese ThingSpeakClient-Klasse ist für die Kommunikation mit ThingSpeak verantwortlich.
            Parameter:  databaseobject: Ein Datenbankobjekt, das die Daten für das Update bereitstellt.
        """

        self.base_url = 'https://api.thingspeak.com/update?api_key='
        self.api_key = os.getenv("apikey")

        # Zeitintervall für den Timer in Sekunden
        self.timer_interval = 59 

        # Erzeugen der benötigten Objekte
        self.databaseobject = databaseobject



    # Ruft den Status ab und sendet ihn an ThingSpeak, setzt dann den Timer, 
    # um sich selbst nach `timer_interval` Sekunden erneut aufzurufen.
    #
    def post_status_periodically(self):
        while True:
            # Manuell den Lock erwerben
            #self.databaseobject.db_lock.acquire()
            #try:
            self.send_status_to_thingspeak()
            #finally:
                # Stellen Sie sicher, dass der Lock immer freigegeben wird
                #self.databaseobject.db_lock.release()

            # Sleep außerhalb des Locks
            sleep(self.timer_interval)



    # Sendet den aktuellen Status des Paketkastens an ThingSpeak.
    #
    def send_status_to_thingspeak(self):
        # Status vom Paketkasten abfragen
        status = self.databaseobject.get_packagebox_status()

        print("Status für ThingSpeak: " + str(status))
        
        if str(status) == "0":
            boxstatus = "Inaktiv"
        elif str(status) == "1":
            boxstatus = "Aktiv"  
        elif str(status) == "2":
            boxstatus = "Fehlertaster"
        elif str(status) == "3":
            boxstatus = "Fehler"
        
        # Daten für das Update vorbereiten
        intvalue = '&field1={}'.format(status)  

        strvalue = '&status={}'.format(boxstatus)  

        # Vollständige URL zusammenstellen
        url = self.base_url + self.api_key + intvalue + strvalue 

        # Anfrage an ThingSpeak senden
        response = urllib.request.urlopen(url)