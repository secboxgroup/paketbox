#!/bin/bash
#
# Aktualisieren der Paketlisten und Upgrade der installierten Pakete
sudo apt-get update && sudo apt-get upgrade -y

# Installation von MariaDB
sudo apt-get install mariadb-server -y

# Ausführen des Datenbank-Erstellungs-Skripts
sudo mariadb < /home/pi/paketbox/Installation/src/PI3/DATABASE_CREATER.sql

# Installation von Mosquitto
sudo apt-get install mosquitto -y

# Stoppen des Mosquitto-Dienstes, um die Konfiguration sicher zu ersetzen
sudo systemctl stop mosquitto

# Ersetzen der Mosquitto-Konfigurationsdatei
sudo cp /home/pi/paketbox/Installation/config/mosquitto.conf /etc/mosquitto/mosquitto.conf

# Neustart des Mosquitto-Dienstes, um die neue Konfiguration zu laden
sudo systemctl start mosquitto

# Installation von nginx
sudo apt-get install nginx -y

# Stoppen des nginx-Dienstes, um die Konfiguration sicher zu ersetzen
sudo systemctl stop nginx

# Ersetzen der nginx-Konfigurationsdatei
sudo cp /home/pi/paketbox/Installation/config/default /etc/nginx/sites-enabled/default

# Neustart des nginx-Dienstes, um die neue Konfiguration zu laden
sudo systemctl start nginx

# Installation von Python3-pip und Virtualenv
sudo apt-get install python3-pip -y
pip3 install --user virtualenv

# Erstellen eines Virtualenvs im gewünschten Verzeichnis
/home/pi/.local/bin/virtualenv /home/pi/venv

# Aktivieren des Virtualenvs und Installation der Abhängigkeiten
source /home/pi/venv/bin/activate
pip install -r /home/pi/paketbox/Installation/src/PI3/requirements.txt
deactivate

# Erstellen des udev-Regel-Ordners, falls nicht vorhanden
sudo mkdir -p /etc/udev/rules.d/

# Erstellen der 99-hidraw-permissions.rules Datei und Hinzufügen der Regel
echo 'KERNEL=="hidraw*", MODE="0666"' | sudo tee /etc/udev/rules.d/99-hidraw-permissions.rules > /dev/null

# Neuladen der udev-Regeln und Auslösen der Regel
sudo udevadm control --reload-rules && sudo udevadm trigger

# Erstellen der systemd Service-Datei für das Python-Skript
echo "[Unit]
Description=Start MAIN.py at boot
After=network-online.target nginx.service mariadb.service wpa_supplicant.service systemd-timesyncd.service multi-user.target
Wants=network-online.target
Requires=nginx.service mariadb.service

[Service]
Type=simple
User=pi
ExecStart=/bin/bash -c 'source /home/pi/venv/bin/activate && python /home/pi/paketbox/Installation/src/PI3/MAIN.py'
Restart=on-failure
ExecStartPre=/bin/sleep 10
[Install]
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/my_python_script.service > /dev/null

# Aktivieren des Services, sodass er beim Booten startet
sudo systemctl daemon-reload
sudo systemctl enable my_python_script.service
sudo systemctl start my_python_script.service

echo "Installation und Konfiguration abgeschlossen."
