
#ifndef App_h
#define App_h

#include <WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"
#include <Adafruit_Sensor.h>
#include "Config.h" 

class App {
public:
    App();
    void setup();
    void loop();

private:
    void setup_wifi();
    void reconnect();
    static void callback(char* topic, byte* message, unsigned int length);

    bool lastState = LOW;
    bool flank = false;
    static bool Erstesendung;
    bool sender = false;

    static DHT dht;

    static WiFiClient espClient;
    static PubSubClient client;
    long lastMsg = 0;
};

#endif
