#include "App.h"

App app; // Erstellen einer Instanz der App-Klasse

// Aufruf der Setup-Methode der App-Klasse
void setup() {
  app.setup(); 
}

// Aufruf der Loop-Methode der App-Klasse
void loop() {
  app.loop(); 
}

// Hauptprogramm-Einstiegspunkt
// Initialisierung
 // Endlosschleife
int main() {
  setup(); 
  
  // Endlosschleife
  while (true) {
    loop();
  }
}
