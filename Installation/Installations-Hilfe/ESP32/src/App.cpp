
#include "App.h"
#include "Config.h"
bool App::Erstesendung = false; // Initialisieren der statischen Variable
DHT App::dht(DHTPIN, DHT11); // Initialisierung des DHT-Sensors
WiFiClient App::espClient;
PubSubClient App::client(espClient);

App::App() {}
// Wifi Setup Medthode 

void App::setup_wifi() {
    Serial.begin(115200);
    Serial.println();
    Serial.print("Verbinde mit ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi verbunden");
    Serial.println("IP-Adresse: ");
    Serial.println(WiFi.localIP());
}



// Empfangen der MQTT Nachrichten und ausführen der Aktionen auf den Verschiedenen Topics 
void App::callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;

  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }

// Empfangen der Erstsendung das beim Start direkt eine Temperatur auf dem Website angezeigt werden kann
  Serial.println();
   if (String(topic) == "SYS/PI/TEMPERATURE") {
    if(messageTemp == "True"){
      Erstesendung = true;
    }  
  } 

//Hören auf das Topic Licht Bei True Licht eimschalten bei False ausschalten 
  if (String(topic) == "SYS/PI/LIGHT") {
    if(messageTemp == "True"){
      Serial.print("light TRUE");
      digitalWrite(light, HIGH);
      client.publish("SYS/ESP/LIGHT", "True");
    }
    else if(messageTemp == "False"){
      Serial.println("light FALSE");
      digitalWrite(light, LOW);
      client.publish("SYS/ESP/LIGHT", "False");
    }
  }

 //Hören auf das Topic Door bei True Türe Summer aktivieren und bei False Summer deaktivieren  
  if (String(topic) == "SYS/PI/DOOR") {
    if(messageTemp == "True"){
      Serial.println("-> door open");
      digitalWrite(door, HIGH);
      client.publish("SYS/ESP/DOOR", "True");
    }
    else if(messageTemp == "False"){
      Serial.println("door close");
      digitalWrite(door, LOW);
      client.publish("SYS/ESP/DOOR", "False");
    }
  }
}


//Automatischen wieder verbinden zum MQTT schlägt es erneut Fehl wird bis zum Nächsten Verbindungs Versuch 5s gewartet 
void App::reconnect() {
    while (!client.connected()) {
        Serial.print("Versuche, MQTT-Verbindung herzustellen...");
        // Versuch, die Verbindung herzustellen
        if (client.connect(device_name)) {
            Serial.println("verbunden");
            // Abonnieren von Topics
            client.subscribe("SYS/#");
        } else {
            Serial.print("fehlgeschlagen, rc=");
            Serial.print(client.state());
            Serial.println(" versuche es in 5 Sekunden erneut");
            delay(5000);
        }
    }
}
//Die Setup Methode
void App::setup() {
    setup_wifi();
    client.setServer(mqtt_server, 1883);
    client.setCallback(callback);

    pinMode(door, OUTPUT);
    pinMode(light, OUTPUT);
    pinMode(button, INPUT_PULLDOWN);

    dht.begin();
}
// Dauerhafte Abfrage ob eine Steigende Flanke am Button erkannt wird 
// Falls eine Positive Flanke erkannt wird so wird auf dem Topic SYS/ESP/BELL ein True veröffentlicht 
void App::loop() {
    if (!client.connected()) {
        reconnect();
    }
    client.loop();

    bool currentState = digitalRead(button);
    if (currentState == HIGH && lastState == LOW) {
        flank = true;
    } else {
        flank = false;
    }
    lastState = currentState;

    if (flank) {
        if (!sender) {
            Serial.println("Taster wurde gedrückt");
            client.publish("SYS/ESP/BELL", "True");
            sender = true;
        }
    } else if (currentState == LOW) {
        sender = false;
    }

// Einlesen alle 15s der Werte des DHT Sensors oder wenn die Erstsendung True ist 
  long now = millis();
  if (now - lastMsg > 15000 or Erstesendung == true) {
    lastMsg = now;

    int h = dht.readHumidity();
    int t = dht.readTemperature();
    Serial.println(h);
    Serial.println(t);

// Überprüfen ob der Sensor einen Wert ausgibt 
    if (isnan(h) || isnan(t)) {
      //  Serial.println(h);
        Serial.println(t);
      Serial.println(F("Failed to read from DHT sensor!"));
      return;
    }

// Versenden des Temperatur Wertes auf dem Topic SYS/ESP/TEMPERATURE 
    Serial.print(F("%  Temperature: "));
    Serial.print(t); // geändert, um t anstelle von hic zu verwenden
    Serial.print(F("°C "));
    char Temp[8];
    dtostrf(t, 2, 0, Temp); 
    client.publish("SYS/ESP/TEMPERATURE", Temp);
    Serial.println();
    Erstesendung = false;
  }
}