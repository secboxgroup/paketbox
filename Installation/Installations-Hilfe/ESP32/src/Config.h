
#ifndef Config_h
#define Config_h

extern const char* ssid;
extern const char* password;
extern const char* mqtt_server;
extern const char* mqtt_topic;
extern const char* device_name;

extern const int door;
extern const int light;
extern const int button;
extern const int DHTPIN;

#endif

