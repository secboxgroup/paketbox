# iSystem2023 - Paketbox


## Über das Projekt

Das iSystem2023 - Paketbox ist eine innovative Lösung, entwickelt von Gruppe 33 "SecBox" der DHBW Mosbach, um die Zustellung und Lagerung von Paketen sicherer, effizienter und benutzerfreundlicher zu gestalten. Dieses System kombiniert moderne Softwaretechnologie mit robuster Hardware, um eine autarke Paketbox zu realisieren, die in jedem Wohngebäude integriert werden kann.


## Hauptmerkmale

- **Benutzerfreundliche Weboberfläche**: Ermöglicht die einfache Verwaltung und Überwachung von Paketlieferungen.
- **Automatisierte Paketannahme**: Durch einen integrierten Scanner und Aktoren für die Türöffnung.
- **Echtzeit-Statusupdates**: Überwachung in Echtzeit über die Weboberfläche und Benachrichtigungssystem.
- **Erweiterte Sicherheitsfunktionen**: Sichere Aufbewahrung von Paketen und Benachrichtigung bei unerwarteten Aktivitäten.
- **Flexible Zugriffssteuerung**: Anpassbare Nutzungszeiten und Fernsteuerung der Paketbox.


## Funktionsweise
Das System funktioniert wie folgt:

1. **Paketannahme**: Der Paketbote scannt den Barcode des Pakets.
   - Das System überprüft die Authentifizität.
   - Bei Nichtregistrierung bleibt die Box verschlossen.
2. **Authentifizierung und Paketablage**: 
   - Bei Erfolg öffnet sich die Box automatisch.
   - Das Paket wird hineingelegt.
3. **Sicherer Verschluss und Benachrichtigung**: 
   - Automatischer Verschluss nach Paketablage.
   - Benachrichtigung an den Benutzer.
4. **Paketentnahme**: 
   - Der Benutzer kann das Paket sicher entnehmen.


## Technologie

Das System basiert auf einem Raspberry Pi (Modell 3 B+) mit Raspbian OS 11 und nutzt eine Kombination aus vordefinierter Hardware, einschließlich Sensoren und Aktoren, für den Betrieb. Die Softwarearchitektur ist modular aufgebaut, um eine einfache Erweiterung und Wartung zu ermöglichen.


## Empfohlene Hardware

- Raspberry Pi 3B+
- Barcodescanner
- Fehler-Taster
- Funkklingel
- LEDs


## Installation

Details zur Installation und Konfiguration des Systems finden sich im beiliegenden Installations- und Betriebshandbuch.


## Nutzung

Eine detaillierte Anleitung zur Benutzung der Paketbox und der Weboberfläche ist im Benutzerhandbuch enthalten.


## Lizenz

Dieses Projekt ist unter einer spezifischen Lizenz veröffentlicht. Details finden Sie in der LICENCE-Datei im Repository.


## Kontakt

- **"Frima"**: GRUPPE33 "SecBox", DHBW Mosbach
- **Ansprechpartner**: Sebastian Mack, Pia Peper, Mergim Doberdolani, Felix Marcinkowski
- **Adresse**: Lohrtalweg 10, 74821 Mosbach


Für weitere Informationen und Unterstützung kontaktieren Sie bitte das Projektteam.

---

© 2024 GRUPPE33 "SecBox", DHBW Mosbach. Alle Rechte vorbehalten.
